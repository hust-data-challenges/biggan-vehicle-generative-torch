mkdir storage
mkdir storage/raw

wget -c https://dl.challenge.zalo.ai/ZAC2019_GAN/training_dataset.zip -d storage/raw
wget -c https://dl.challenge.zalo.ai/ZAC2019_GAN/evaluation_script.zip -d storage/raw
wget -c https://dl.challenge.zalo.ai/ZAC2019_GAN/motor_gen_128.zip -d storage/raw

unzip storage/raw/training_dataset.zip -d storage/raw/
unzip storage/raw/evaluation_script.zip -d storage/raw/
unzip storage/raw/motor_gen_128.zip -d storage/raw/