from libs.solver import Solver
from libs.utils.config_parser import cfg_from_file
from libs.models.model_factory import get_discriminator, get_generator
import tensorflow as tf
from storage.evaluation_script.client.motor_classifier import MotorbikeClassifier
import cv2
import numpy as np
from libs.datasets.vehicle_dataset import VehicleDataset
from libs.datasets.dataset_factory import get_dataloader
import torch
from libs.transformer import get_transform
from tqdm import tqdm


if __name__ == '__main__':
    config = cfg_from_file("yml_files/dcgan_trainning.yml")
#     # # transform1, transform2 = get_transform(config.transform, config.common)
#     # # dataset = VehicleDataset(config.dataset, config.common, transform1=transform1, transform2=transform2)
#     # # # # loader = get_dataloader(config, None, transform1, transform2)
#     # # for i in tqdm(range(len(dataset))):
#     # #     x = dataset[i]
#     # solver = Solver(config)
#     # solver.train_model(n_example_to_gen=2, calculate_mifid=3, config=config)
#     # # from PIL import Image
#     # # from PIL import ImageFile
#     # #
#     # # ImageFile.LOAD_TRUNCATED_IMAGES = True
#     # # img = Image.open('./storage/raw/motobike/2019_08_05_05_17_32_B0xS_6hHgXG_66398352_483445189138958_8195470045202604419_n_1568719912383_18787.jpg')
#     # #
#     # # # img.re
#     # # img = img.convert('RGB')
#     # # img = transform1(img)
#     # # print(img.size())
#     model = get_generator(config.model, config.common)
    l = torch.nn.Linear(config.common.noise_dim,
                        config.common.image_width // config.model.scale_factor * config.common.image_height // config.model.scale_factor * 128)
    noise = torch.randn(64, config.common.noise_dim, 1, 1)
    print(noise.size(), l.weight.size())
    y = l(noise)



