from libs.models.modules import dcgan
from libs.models.modules import biggan

generation_mapper = {
    'dcgan': dcgan.Generator,
    'leaky_biggan': biggan.LGenerator,
    'ss_block_biggan': biggan.ScSEGenerator,
    'leaky_resconcat_biggan': biggan.LResConcatGenerator,
    'leaky_dense_biggan': biggan.LDenseGenerator,
}

discrimination_mapper = {
    'dcgan': dcgan.Discriminator,
    'leaky_biggan': biggan.LDiscriminator,
    'ss_block_biggan': biggan.ScSEDiscriminator,
    'leaky_resconcat_biggan': biggan.LResConcatDiscriminator,
    'leaky_dense_biggan': biggan.LDenseDiscriminator,
}

runner = {
    'default': {
        '4_3': {
            'gen': generation_mapper['leaky_biggan'](n_feat=22, codes_dim=24, n_classes=1),
            'dis': discrimination_mapper['leaky_biggan'](n_feat=26)
        },
        '6_5': {
            'gen': generation_mapper['leaky_biggan'],
            'dis': discrimination_mapper['leaky_biggan']
        },
        '8_7': {
            'gen': generation_mapper['leaky_biggan'],
            'dis': discrimination_mapper['leaky_biggan']
        },
        '10_8': {
            'gen': generation_mapper['leaky_biggan'],
            'dis': discrimination_mapper['leaky_biggan']
        },
    },
    'res_concat': {
        '8_7': {
            'gen': generation_mapper['leaky_biggan'],
            'dis': discrimination_mapper['leaky_biggan']
        },
        '7_6': {
            'gen': generation_mapper['leaky_biggan'],
            'dis': discrimination_mapper['leaky_biggan']
        },
        '6_5': {
        'gen': generation_mapper['leaky_biggan'],
            'dis': discrimination_mapper['leaky_biggan']
        }
    }
}