from libs.utils.training_utils import *


def cross_entropy(**_):
    return torch.nn.BCELoss(**_)


def discriminator_loss(real_output, fake_output, loss_func, apply_label_smoothing=True, label_noise=True):
    real_loss, fake_loss = 0, 0
    if label_noise and apply_label_smoothing:
        real_output_noise = noisy_labels(torch.ones_like(real_output), 0.05)
        fake_output_noise = noisy_labels(torch.zeros_like(fake_output), 0.05)
        real_output_smooth = smooth_positive_labels(real_output_noise)
        fake_output_smooth = smooth_negative_labels(fake_output_noise)
        if loss_func == 'gan':
            real_loss = cross_entropy()(torch.ones_like(real_output_smooth), real_output)
            fake_loss = cross_entropy()(torch.zeros_like(fake_output_smooth), fake_output)
        else:
            if loss_func == 'ralsgan':
                d_loss = real_output_smooth - fake_output_smooth.mean() - torch.ones_like(real_output_smooth)
                g_loss = fake_output_smooth - real_output_smooth.mean() + torch.ones_like(fake_output_smooth)
                d_loss = d_loss * d_loss
                g_loss = g_loss * g_loss
                return (d_loss.mean() + g_loss.mean()) / 2.
            elif loss_func == 'rasgan':
                avg_fake_logit = fake_output_smooth.mean()
                avg_real_logit = real_output_smooth.mean()
                D_r_tilde = torch.sigmoid(real_output_smooth - avg_fake_logit)
                D_f_tilde = torch.sigmoid(fake_output_smooth - avg_real_logit)
                total_loss = - torch.log(D_r_tilde + 1e-14).mean() - torch.log(-D_f_tilde + 1e-14 + 1).mean()
                return total_loss
            elif loss_func == 'rahinge':
                real_loss = (torch.relu(
                    torch.ones_like(real_output_smooth) - (real_output_smooth - fake_output_smooth.mean()))).mean()
                fake_loss = (torch.relu(
                    torch.ones_like(fake_output_smooth) + (fake_output_smooth - real_output_smooth.mean()))).mean()
        total_loss = real_loss + fake_loss
        return total_loss
    elif label_noise and not apply_label_smoothing:
        real_output_noise = noisy_labels(torch.ones_like(real_output), 0.05)
        fake_output_noise = noisy_labels(torch.zeros_like(fake_output), 0.05)
        if loss_func == 'gan':
            real_loss = cross_entropy()(torch.ones_like(real_output_noise), real_output)
            fake_loss = cross_entropy()(torch.zeros_like(fake_output_noise), fake_output)
        else:
            if loss_func == 'ralsgan':
                d_loss = real_output_noise - fake_output_noise.mean() - torch.ones_like(real_output_noise)
                g_loss = fake_output_noise - real_output_noise.mean() + torch.ones_like(fake_output_noise)
                d_loss = d_loss * d_loss
                g_loss = g_loss * g_loss
                return (d_loss.mean() + g_loss.mean()) / 2.
            elif loss_func == 'rasgan':
                avg_fake_logit = fake_output_noise.mean()
                avg_real_logit = real_output_noise.mean()
                D_r_tilde = torch.sigmoid(real_output_noise - avg_fake_logit)
                D_f_tilde = torch.sigmoid(fake_output_noise - avg_real_logit)
                total_loss = - torch.log(D_r_tilde + 1e-14).mean() - torch.log(-D_f_tilde + 1e-14 + 1).mean()
                return total_loss
            elif loss_func == 'rahinge':
                real_loss = (torch.relu(
                    torch.ones_like(real_output_noise) - (real_output_noise - fake_output_noise.mean()))).mean()
                fake_loss = (torch.relu(
                    torch.ones_like(fake_output_noise) + (fake_output_noise - real_output_noise.mean()))).mean()
        total_loss = real_loss + fake_loss
        return total_loss
    elif apply_label_smoothing and not label_noise:
        real_output_smooth = smooth_positive_labels(torch.ones_like(real_output))
        fake_output_smooth = smooth_negative_labels(torch.zeros_like(fake_output))
        if loss_func == 'gan':
            real_loss = cross_entropy()(torch.ones_like(real_output_smooth), real_output)
            fake_loss = cross_entropy()(torch.zeros_like(fake_output_smooth), fake_output)
        else:
            if loss_func == 'ralsgan':
                d_loss = real_output_smooth - fake_output_smooth.mean() - torch.ones_like(real_output_smooth)
                g_loss = fake_output_smooth - real_output_smooth.mean() + torch.ones_like(fake_output_smooth)
                d_loss = d_loss * d_loss
                g_loss = g_loss * g_loss
                return (d_loss.mean() + g_loss.mean()) / 2.

            elif loss_func == 'rasgan':
                avg_fake_logit = fake_output_smooth.mean()
                avg_real_logit = real_output_smooth.mean()
                D_r_tilde = torch.sigmoid(real_output_smooth - avg_fake_logit)
                D_f_tilde = torch.sigmoid(fake_output_smooth - avg_real_logit)
                total_loss = - torch.log(D_r_tilde + 1e-14).mean() - torch.log(-D_f_tilde + 1e-14 + 1).mean()
                return total_loss
            elif loss_func == 'rahinge':
                real_loss = (torch.relu(
                    torch.ones_like(real_output_smooth) - (real_output_smooth - fake_output_smooth.mean()))).mean()
                fake_loss = (torch.relu(
                    torch.ones_like(fake_output_smooth) + (fake_output_smooth - real_output_smooth.mean()))).mean()
        total_loss = real_loss + fake_loss
        return total_loss
    else:
        if loss_func == 'gan':
            real_loss = cross_entropy()(torch.ones_like(real_output), real_output)
            fake_loss = cross_entropy()(torch.zeros_like(fake_output), fake_output)
        else:
            if loss_func == 'ralsgan':
                d_loss = real_output - fake_output.mean() - torch.ones_like(real_output)
                g_loss = fake_output - real_output.mean() + torch.ones_like(fake_output)
                d_loss = d_loss * d_loss
                g_loss = g_loss * g_loss
                return (d_loss.mean() + g_loss.mean()) / 2.

            elif loss_func == 'rasgan':
                avg_fake_logit = fake_output.mean()
                avg_real_logit = real_output.mean()
                D_r_tilde = torch.sigmoid(real_output - avg_fake_logit)
                D_f_tilde = torch.sigmoid(fake_output - avg_real_logit)
                total_loss = - torch.log(D_r_tilde + 1e-14).mean() - torch.log(-D_f_tilde + 1e-14 + 1).mean()
                return total_loss
            elif loss_func == 'rahinge':
                real_loss = (torch.relu(torch.ones_like(real_output) - (real_output - fake_output.mean()))).mean()
                fake_loss = (torch.relu(torch.ones_like(fake_output) + (fake_output - real_output.mean()))).mean()
        total_loss = real_loss + fake_loss
        return total_loss


def generator_loss(real_output, fake_output, loss_func, apply_label_smoothing=True):
    if apply_label_smoothing:
        fake_output_smooth = smooth_negative_labels(torch.ones_like(fake_output))
        if loss_func == 'gan':
            return cross_entropy()(torch.ones_like(fake_output_smooth), fake_output)
        else:
            if loss_func == 'ralsgan':
                d_loss = real_output - fake_output_smooth.mean() + torch.ones_like(real_output)
                g_loss = fake_output_smooth - real_output.mean() - torch.ones_like(fake_output_smooth)
                d_loss = d_loss * d_loss
                g_loss = g_loss * g_loss
                return (d_loss.mean() + g_loss.mean()) / 2.

            elif loss_func == 'rasgan':
                avg_fake_logit = fake_output_smooth.mean()
                avg_real_logit = real_output.mean()
                D_r_tilde = torch.sigmoid(real_output - avg_fake_logit)
                D_f_tilde = torch.sigmoid(fake_output_smooth - avg_real_logit)
                total_loss = - torch.log(D_f_tilde + 1e-14).mean() - torch.log(-D_r_tilde + 1e-14 + 1).mean()
                return total_loss
            elif loss_func == 'rahinge':
                fake_loss = (
                    torch.relu(torch.ones_like(fake_output_smooth) - (fake_output_smooth - real_output.mean()))).mean()
                real_loss = (
                    torch.relu(torch.ones_like(real_output) + (real_output - fake_output_smooth.mean()))).mean()
                total_loss = fake_loss + real_loss
                return total_loss
    else:
        if loss_func == 'gan':
            return cross_entropy()(torch.ones_like(fake_output), fake_output)
        else:
            if loss_func == 'ralsgan':
                d_loss = real_output - fake_output.mean() + torch.ones_like(real_output)
                g_loss = fake_output - real_output.mean() - torch.ones_like(fake_output)
                d_loss = d_loss * d_loss
                g_loss = g_loss * g_loss
                return (g_loss.mean() + d_loss.mean()) / 2.
            elif loss_func == 'rasgan':
                avg_fake_logit = fake_output.mean()
                avg_real_logit = real_output.mean()
                D_r_tilde = torch.sigmoid(real_output - avg_fake_logit)
                D_f_tilde = torch.sigmoid(fake_output - avg_real_logit)
                total_loss = - torch.log(D_f_tilde + 1e-14).mean() - torch.log(-D_r_tilde + 1e-14 + 1).mean()
                return total_loss
            elif loss_func == 'rahinge':
                fake_loss = (torch.relu(torch.ones_like(fake_output) - (fake_output - real_output.mean()))).mean()
                real_loss = (torch.relu(torch.ones_like(real_output) + (real_output - fake_output.mean()))).mean()
                total_loss = fake_loss + real_loss
                return total_loss


def get_loss(loss_type, **_):
    return globals().get(loss_type)


if __name__ == '__main__':
    loss = get_loss("discriminator_loss")
