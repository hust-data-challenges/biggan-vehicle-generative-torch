from libs.losses.losses import generator_loss, discriminator_loss

def get_loss(loss_type, **_):
    return globals().get(loss_type)


if __name__ == '__main__':
    loss = get_loss("discriminator_loss")
