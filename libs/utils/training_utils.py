import matplotlib.pyplot as plt
import numpy as np
import torch

device = torch.device("cuda:0" if torch.cuda.is_available() else "cpu")


# Label smoothing -- technique from GAN hacks, instead of assigning 1/0 as class labels, we assign a random integer in range [0.7, 1.0] for positive class
# and [0.0, 0.3] for negative class
def smooth_positive_labels(y):
    return y - 0.3 + (np.random.random(y.shape) * 0.5)


def smooth_negative_labels(y):
    return y + np.random.random(y.shape) * 0.3


def noisy_labels(y, p_flip):
    # determine the number of labels to flip
    n_select = int(p_flip * int(y.shape[0]))
    # choose labels to flip
    flip_ix = np.random.choice([i for i in range(int(y.shape[0]))], size=n_select)

    op_list = []
    # invert the labels in place
    # y_np[flip_ix] = 1 - y_np[flip_ix]
    for i in range(int(y.shape[0])):
        if i in flip_ix:
            op_list.append(1.0 - y[i])
        else:
            op_list.append(y[i])

    outputs = torch.stack(op_list)
    return outputs


def count_parameters(model):
    return sum(p.numel() for p in model.parameters() if p.requires_grad)


def plot_losses(G_losses, D_losses, all_gl, all_dl, epoch):
    plt.figure(figsize=(10, 5))
    plt.title("Generator and Discriminator Loss - EPOCH {}".format(epoch))
    plt.plot(G_losses, label="G")
    plt.plot(D_losses, label="D")
    plt.xlabel("Iterations")
    plt.ylabel("Loss")
    plt.legend()
    ymax = plt.ylim()[1]
    plt.show()

    plt.figure(figsize=(10, 5))
    plt.plot(np.arange(len(all_gl)), all_gl, label='G')
    plt.plot(np.arange(len(all_dl)), all_dl, label='D')
    plt.legend()
    # plt.ylim((0,np.min([1.1*np.max(all_gl),2*ymax])))
    plt.title('All Time Loss')
    plt.show()


def generate_test_image(model, noise_dim):
    model.test()
    test_input = torch.randn(1, noise_dim, device=device)
    # Notice `training` is set to False.
    # This is so all layers run in inference mode (batchnorm).
    predictions = model(test_input)
    fig = plt.figure(figsize=(5, 5))
    plt.imshow((predictions[0, :, :, :] * 127.5 + 127.5) / 255.)
    plt.axis('off')
    plt.show()
