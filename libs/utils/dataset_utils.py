import os

import cv2
import numpy as np
from PIL import Image, ImageSequence
from tqdm import tqdm
from PIL import ImageFile

ImageFile.LOAD_TRUNCATED_IMAGES = True


def read_image(path):
    try:
        img = cv2.imread(path)
        img = cv2.cvtColor(img, cv2.COLOR_BGR2RGB)
        if img.shape[2] == 1:
            img2 = np.zeros(shape=[img.shape[1], img.shape[2], 3])
            img2[:, :, 0] = img
            img2[:, :, 1] = img
            img2[:, :, 2] = img
            img = img2
        img = Image.fromarray(img)
    except:
        img = Image.open(path)
        if path.split('.')[-1] == 'gif':
            for frame in ImageSequence.Iterator(img):
                #             frame.save("frame%d.png" % index)
                img = cv2.cvtColor(np.array(frame), cv2.COLOR_BGR2RGB)
                break
            img = Image.fromarray(img)
        # else:
        #     img = img.convert('RGB')
        #     img = np.array(img)
            # Convert RGB to BGR
            # img = img[:, :, ::-1]
    return img

def PIL_read(path):
    img = Image.open(path, mode='r')
    try:
        img = img.convert('RGB')
    except:
        print(path)
    # if path.split('.')[-1] == 'gif':
    #     for frame in ImageSequence.Iterator(img):
    #         #             frame.save("frame%d.png" % index)
    #         img = cv2.cvtColor(np.array(frame), cv2.COLOR_RGB2BGR)
    #         break
    # else:
    #     img = img.convert('RGB')
    #     img = np.array(img)
    #     # Convert RGB to BGR
    #     img = img[:, :, ::-1]
    return img


def prepare_dataset(config, common, sample_size=25000, **_):
    npz_file = config.npz_file
    image_width = common.image_width
    image_height = common.image_height
    image_channels = common.image_channels
    preprocesing_fn = None
    if os.path.isfile(npz_file):
        with np.load(npz_file) as data_file:
            return data_file["data"]

    vehicle_images_np = np.zeros((sample_size, image_width, image_height, image_channels))
    idx = -1
    for path_ in tqdm(os.listdir(config.image_input_dir)):
        img_path = config.image_input_dir + path_
        img = read_image(img_path)
        if preprocesing_fn:
            img = preprocesing_fn(img)

        if True:
            interpolation = cv2.INTER_AREA
        else:
            interpolation = cv2.INTER_CUBIC

        img = cv2.resize(img, (image_width, image_height),
                         interpolation=interpolation)
        idx += 1
        vehicle_images_np[idx, :, :, :] = img
    if npz_file:
        np.savez_compressed(
            npz_file,
            data=vehicle_images_np
        )
    return vehicle_images_np
