import numpy as np
from PIL import ImageOps, ImageEnhance


def autocontrast(img, cutoff=1):  # cutoff[%]
    if np.random.rand() < 0.5:
        img = ImageOps.autocontrast(img, cutoff)
    return img


def sharpen(img, magnitude=1):
    factor = np.random.uniform(1.0 - magnitude, 1.0 + magnitude)
    img = ImageEnhance.Sharpness(img).enhance(factor)
    return img
