import os

import matplotlib.pyplot as plt
import numpy as np


def write_logs(logger, title, value, epoch):
    logger.scalar_summary(title, value, step=epoch)


def make_dir(path):
    try:
        os.makedirs(path)
        print("Directory ", path, " Created ")
    except FileExistsError:
        print("Directory ", path, " already exists")


def make_all_dirs(config):
    dirs = [config.common.logs_dir + 'generation_logs/', config.common.logs_dir + 'discrimination_logs/',
            config.common.exp_dir, config.common.resume_checkpoint]
    for dir in dirs:
        make_dir(dir)


def gen_and_save(model, epoch, test_input, rows, cols, is_plot=False):
    # Notice `training` is set to False.
    # This is so all layers run in inference mode (batchnorm).
    predictions = model(test_input, training=False)
    fig = plt.figure(figsize=(14, 14))
    for i in range(predictions.shape[0]):
        plt.subplot(rows, cols, i + 1)
        plt.imshow((predictions[i, :, :, :] * 127.5 + 127.5) / 255.)
        plt.axis('off')

    plt.subplots_adjust(wspace=0, hspace=0)
    plt.savefig('png/image_at_epoch_{:04d}.png'.format(epoch))
    if is_plot:
        plt.show()


def plot_losses(G_losses, D_losses, all_gl, all_dl, epoch):
    plt.figure(figsize=(10, 5))
    plt.title("Generator and Discriminator Loss - EPOCH {}".format(epoch))
    plt.plot(G_losses, label="G")
    plt.plot(D_losses, label="D")
    plt.xlabel("Iterations")
    plt.ylabel("Loss")
    plt.legend()
    ymax = plt.ylim()[1]
    plt.savefig("Generator-Discriminator-Loss-EPOCH-{}.png".format(epoch))
    # plt.show()

    plt.figure(figsize=(10, 5))
    plt.plot(np.arange(len(all_gl)), all_gl, label='G')
    plt.plot(np.arange(len(all_dl)), all_dl, label='D')
    plt.legend()
    # plt.ylim((0,np.min([1.1*np.max(all_gl),2*ymax])))
    plt.savefig('All-Time-Loss.png')
    # plt.show()

def get_str(FID, loss1, loss2):
    return '_FID_' + str(FID) + '_gloss_' + str(loss1) + '_dloss_' + str(loss2)

def get_row_summaryDF():
    """
    generator discriminator g_params d_params epoch default_params training_type label_noise type_loss g_loss d_loss FID
    :return:
    """
    pass