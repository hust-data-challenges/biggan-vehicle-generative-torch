import os
from glob import glob

from torch.utils.data.dataset import Dataset
from libs.utils.dataset_utils import read_image, PIL_read


class VehicleDataset(Dataset):
    def __init__(self, dataset_config, common_config, preprocessor=None, transform1=None, transform2=None):
        self.transform1 = transform1
        self.transform2 = transform2
        self.preprocessor = preprocessor
        self.dataset_config = dataset_config
        self.common_config = common_config

        self.image_list = os.listdir(dataset_config.dataset_root)
        self.image_list = [dataset_config.dataset_root + path for path in self.image_list]
        self.images = []
        # for i in len(self.image_list):
        #

    def __len__(self):
        return len(self.image_list)

    def __getitem__(self, index):
        if index == 0:
            path = self.image_list[0]
        else:
            path = self.image_list[index % len(self.image_list)]
        try:
            image = PIL_read(path)
            if self.preprocessor:
                image = self.preprocessor(image)

            if self.transform1:
                image = self.transform1(image)

            if self.transform2:
                image = self.transform2(image)

            if image.shape[0] != 3:
                print(path)
        except:
            print(path)

        return image