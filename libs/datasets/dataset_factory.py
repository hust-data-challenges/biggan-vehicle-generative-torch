from torch.utils.data import DataLoader
from libs.datasets.vehicle_dataset import VehicleDataset


def get_dataset(config, preprocessor=None,  transform1=None, transform2=None):
    print(config.dataset.type)
    f = globals().get(config.dataset.type)
    return f(config.dataset, config.common, preprocessor,  transform1, transform2)


def get_dataloader(config, preprocessor=None, transform1=None, transform2=None):
    dataset = get_dataset(config, preprocessor, transform1, transform2)
    is_train = config.common.is_train
    dataloader = DataLoader(dataset,
                            shuffle=is_train,
                            batch_size=config.train.batch_size,
                            drop_last=is_train,
                            num_workers=config.common.num_preprocessor,
                            pin_memory=False)
    return dataloader
