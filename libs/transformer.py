from torchvision import transforms

def get_transform(config, common):

    # transform1 = transforms.Compose([transforms.Resize(common.image_width)])
    transform1 = None
    transform2 = transforms.Compose([transforms.RandomCrop(common.image_width),
                                     transforms.RandomHorizontalFlip(p=config.p),
                                     transforms.ToTensor(),
                                     transforms.Normalize(mean=[config.MEAN1, config.MEAN2, config.MEAN3],
                                                          std=[config.STD1, config.STD2, config.STD3]),
                                     ])
    return transform1, transform2