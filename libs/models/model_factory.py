from libs.models.modules import dcgan
from libs.models.modules import biggan

generation_mapper = {
    'dcgan': dcgan.Generator,
    'leaky_biggan': biggan.LGenerator,
    'ss_block_biggan': biggan.ScSEGenerator,
    'leaky_resconcat_biggan': biggan.LResConcatGenerator,
    'leaky_dense_biggan': biggan.LDenseGenerator,
}

discrimination_mapper = {
    'dcgan': dcgan.Discriminator,
    'leaky_biggan': biggan.LDiscriminator,
    'ss_block_biggan': biggan.ScSEDiscriminator,
    'leaky_resconcat_biggan': biggan.LResConcatDiscriminator,
    'leaky_dense_biggan': biggan.LDenseDiscriminator,
}


def get_discriminator(config, common):
    model = discrimination_mapper[config.discriminator](config, common)
    return model


def get_generator(config, common, ema_model=False):
    model = generation_mapper[config.generator](config, common)
    return model
