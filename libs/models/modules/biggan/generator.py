from libs.models.modules.utils import LResBlock_G, Attention, init_weight, conv3x3, \
    LResConcat_ResBlock_G, DenseBlock_G, ScSEBlock_ResBlock_G, Transition_G, scSEBlock
import torch
import torch.nn as nn


class LGenerator(nn.Module):
    def __init__(self, config, common):
        super().__init__()
        n_feat = common.n_feat
        codes_dim = common.codes_dim
        self.codes_dim = codes_dim
        self.fc = nn.Sequential(
            nn.utils.spectral_norm(nn.Linear(codes_dim, 16 * n_feat * 4 * 4)).apply(init_weight)
        )
        self.res1 = LResBlock_G(16 * n_feat, 16 * n_feat, codes_dim + 1, upsample=True)
        self.res2 = LResBlock_G(16 * n_feat, 8 * n_feat, codes_dim + 1, upsample=True)
        self.res3 = LResBlock_G(8 * n_feat, 4 * n_feat, codes_dim + 1, upsample=True)
        self.attn = Attention(4 * n_feat)
        self.res4 = LResBlock_G(4 * n_feat, 2 * n_feat, codes_dim + 1, upsample=True)
        self.conv = nn.Sequential(
            # nn.BatchNorm2d(2*n_feat).apply(init_weight),
            nn.LeakyReLU(),
            nn.utils.spectral_norm(conv3x3(2 * n_feat, 3)).apply(init_weight),
        )

    def forward(self, z, label_ohe):
        """
        z.shape = (*,Z_DIM)
        label_ohe.shape = (*,n_classes)
        """
        batch = z.size(0)
        z = z.squeeze()
        label_ohe = label_ohe.squeeze()
        codes = torch.split(z, self.codes_dim, dim=1)

        x = self.fc(codes[0])  # ->(*,16ch*4*4)
        x = x.view(batch, -1, 4, 4)  # ->(*,16ch,4,4)

        condition = torch.cat([codes[1], label_ohe], dim=1)  # (*,codes_dim+n_classes)
        x = self.res1(x, condition)  # ->(*,16ch,8,8)

        condition = torch.cat([codes[2], label_ohe], dim=1)
        x = self.res2(x, condition)  # ->(*,8ch,16,16)

        condition = torch.cat([codes[3], label_ohe], dim=1)
        x = self.res3(x, condition)  # ->(*,4ch,32,32)
        x = self.attn(x)  # not change shape

        condition = torch.cat([codes[4], label_ohe], dim=1)
        x = self.res4(x, condition)  # ->(*,2ch,64,64)

        x = self.conv(x)  # ->(*,3,64,64)
        x = torch.tanh(x)
        return x


# BigGAN + scSEBlock
class ScSEGenerator(nn.Module):
    def __init__(self, config, common, codes_dim=24):
        super().__init__()
        n_feat = common.n_feat
        if codes_dim is None:
            codes_dim = common.codes_dim
        self.fc = nn.Sequential(
            nn.utils.spectral_norm(nn.Linear(codes_dim, 16 * n_feat * 4 * 4)).apply(init_weight),
            # nn.BatchNorm1d(16*n_feat*4*4).apply(init_weight),
            # nn.ReLU(),
        )
        self.res1 = ScSEBlock_ResBlock_G(16 * n_feat, 16 * n_feat, codes_dim + 1, upsample=True)
        self.scse1 = scSEBlock(16 * n_feat, 8, 8)
        self.res2 = ScSEBlock_ResBlock_G(16 * n_feat, 8 * n_feat, codes_dim + 1, upsample=True)
        self.scse2 = scSEBlock(8 * n_feat, 16, 16)
        self.res3 = ScSEBlock_ResBlock_G(8 * n_feat, 4 * n_feat, codes_dim + 1, upsample=True)
        self.scse3 = scSEBlock(4 * n_feat, 32, 32)
        self.res4 = ScSEBlock_ResBlock_G(4 * n_feat, 2 * n_feat, codes_dim + 1, upsample=True)
        self.conv = nn.Sequential(
            nn.BatchNorm2d(2 * n_feat).apply(init_weight),
            nn.ReLU(),
            nn.utils.spectral_norm(conv3x3(2 * n_feat, 3)).apply(init_weight),
        )

    def forward(self, z, label_ohe, codes_dim=24):
        """
        z.shape = (*,120)
        label_ohe.shape = (*,n_classes)
        """
        batch = z.size(0)
        z = z.squeeze()
        label_ohe = label_ohe.squeeze()
        codes = torch.split(z, codes_dim, dim=1)

        x = self.fc(codes[0])  # ->(*,16ch*4*4)
        x = x.view(batch, -1, 4, 4)  # ->(*,16ch,4,4)

        condition = torch.cat([codes[1], label_ohe], dim=1)  # (*,codes_dim+n_classes)
        x = self.res1(x, condition)  # ->(*,16ch,8,8)
        x = self.scse1(x)  # not change shape

        condition = torch.cat([codes[2], label_ohe], dim=1)
        x = self.res2(x, condition)  # ->(*,8ch,16,16)
        x = self.scse2(x)  # not change shape

        condition = torch.cat([codes[3], label_ohe], dim=1)
        x = self.res3(x, condition)  # ->(*,4ch,32,32)
        x = self.scse3(x)  # not change shape

        condition = torch.cat([codes[4], label_ohe], dim=1)
        x = self.res4(x, condition)  # ->(*,2ch,64,64)

        x = self.conv(x)  # ->(*,3,64,64)
        x = torch.tanh(x)
        return x


# BigGAN + leaky_relu + res concat
class LResConcatGenerator(nn.Module):
    def __init__(self, config, common):
        super().__init__()
        n_feat = common.n_feat
        codes_dim = common.codes_dim
        self.codes_dim = codes_dim
        self.fc = nn.Sequential(
            nn.utils.spectral_norm(nn.Linear(codes_dim, 16 * n_feat * 4 * 4)).apply(init_weight)
        )
        self.res1 = LResConcat_ResBlock_G(16 * n_feat, 8 * n_feat, codes_dim + 1, upsample=True)
        self.res2 = LResConcat_ResBlock_G(16 * n_feat, 4 * n_feat, codes_dim + 1, upsample=True)
        self.res3 = LResConcat_ResBlock_G(8 * n_feat, 2 * n_feat, codes_dim + 1, upsample=True)
        self.attn = Attention(4 * n_feat)
        self.res4 = LResConcat_ResBlock_G(4 * n_feat, n_feat, codes_dim + 1, upsample=True)
        self.conv = nn.Sequential(
            nn.BatchNorm2d(2 * n_feat).apply(init_weight),
            nn.LeakyReLU(),
            nn.utils.spectral_norm(conv3x3(2 * n_feat, 3)).apply(init_weight),
        )

    def forward(self, z, label_ohe):
        """
        z.shape = (*,Z_DIM)
        label_ohe.shape = (*,n_classes)
        """
        batch = z.size(0)
        z = z.squeeze()
        label_ohe = label_ohe.squeeze()
        codes = torch.split(z, self.codes_dim, dim=1)

        x = self.fc(codes[0])  # ->(*,16ch*4*4)
        x = x.view(batch, -1, 4, 4)  # ->(*,16ch,4,4)

        condition = torch.cat([codes[1], label_ohe], dim=1)  # (*,codes_dim+n_classes)
        x = self.res1(x, condition)  # ->(*,16ch,8,8)

        condition = torch.cat([codes[2], label_ohe], dim=1)
        x = self.res2(x, condition)  # ->(*,8ch,16,16)

        condition = torch.cat([codes[3], label_ohe], dim=1)
        x = self.res3(x, condition)  # ->(*,4ch,32,32)
        x = self.attn(x)  # not change shape

        condition = torch.cat([codes[4], label_ohe], dim=1)
        x = self.res4(x, condition)  # ->(*,2ch,64,64)

        x = self.conv(x)  # ->(*,3,64,64)
        x = torch.tanh(x)
        return x


# #BigGAN + leaky_relu + DenseBlock
class LDenseGenerator(nn.Module):
    def __init__(self, config, common):
        super().__init__()
        n_feat = common.n_feat
        codes_dim = common.codes_dim
        self.codes_dim = codes_dim
        self.fc = nn.Sequential(
            nn.utils.spectral_norm(nn.Linear(codes_dim, 16 * n_feat * 4 * 4)).apply(init_weight)
        )
        self.res1 = DenseBlock_G(16 * n_feat, 128, 32, codes_dim + 1)
        self.res1_2 = DenseBlock_G(16 * n_feat + 32, 128, 32, codes_dim + 1)
        self.tra1 = Transition_G(16 * n_feat + 64, 8 * n_feat)
        self.res2 = DenseBlock_G(8 * n_feat, 128, 32, codes_dim + 1)
        self.res2_2 = DenseBlock_G(8 * n_feat + 32, 128, 32, codes_dim + 1)
        self.tra2 = Transition_G(8 * n_feat + 64, 4 * n_feat)
        self.res3 = DenseBlock_G(4 * n_feat, 128, 32, codes_dim + 1)
        self.res3_2 = DenseBlock_G(4 * n_feat + 32, 128, 32, codes_dim + 1)
        self.tra3 = Transition_G(4 * n_feat + 64, 2 * n_feat)
        self.attn = Attention(2 * n_feat)
        self.res4 = DenseBlock_G(2 * n_feat, 128, 32, codes_dim + 1)
        self.res4_2 = DenseBlock_G(2 * n_feat + 32, 128, 32, codes_dim + 1)
        self.tra4 = Transition_G(2 * n_feat + 64, n_feat)
        self.conv = nn.Sequential(
            nn.BatchNorm2d(n_feat).apply(init_weight),
            nn.LeakyReLU(),
            nn.utils.spectral_norm(conv3x3(n_feat, 3)).apply(init_weight),
        )

    def forward(self, z, label_ohe):
        """
        z.shape = (*,Z_DIM)
        label_ohe.shape = (*,n_classes)
        """
        batch = z.size(0)
        z = z.squeeze()
        label_ohe = label_ohe.squeeze()
        codes = torch.split(z, self.codes_dim, dim=1)

        x = self.fc(codes[0])  # ->(*,16ch*4*4)
        x = x.view(batch, -1, 4, 4)  # ->(*,16ch,4,4)

        condition = torch.cat([codes[1], label_ohe], dim=1)  # (*,codes_dim+n_classes)
        x = self.res1(x, condition)
        x = self.res1_2(x, condition)
        x = self.tra1(x)  # ->(*,16ch,8,8)

        condition = torch.cat([codes[2], label_ohe], dim=1)
        x = self.res2(x, condition)
        x = self.res2_2(x, condition)
        x = self.tra2(x)  # ->(*,8ch,16,16)

        condition = torch.cat([codes[3], label_ohe], dim=1)
        x = self.res3(x, condition)
        x = self.res3_2(x, condition)
        x = self.tra3(x)  # ->(*,4ch,32,32)
        x = self.attn(x)  # not change shape

        condition = torch.cat([codes[4], label_ohe], dim=1)
        x = self.res4(x, condition)
        x = self.res4_2(x, condition)
        x = self.tra4(x)  # ->(*,2ch,64,64)

        x = self.conv(x)  # ->(*,3,64,64)
        x = torch.tanh(x)
        return x
