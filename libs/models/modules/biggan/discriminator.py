import torch
import torch.nn as nn
import torch.nn.functional as F
from libs.models.modules.utils import LResBlock_D, Attention, init_weight, \
    LResConcat_ResBlock_D, DenseBlock_D, ScSEBlock_ResBlock_D, Transition_D, scSEBlock, sSEBlock


class LDiscriminator(nn.Module):
    def __init__(self, config, common):
        super().__init__()
        n_feat = common.n_feat
        self.res1 = LResBlock_D(3, n_feat, downsample=True)
        self.attn = Attention(n_feat)
        self.res2 = LResBlock_D(n_feat, 2 * n_feat, downsample=True)
        self.res3 = LResBlock_D(2 * n_feat, 4 * n_feat, downsample=True)
        self.res4 = LResBlock_D(4 * n_feat, 8 * n_feat, downsample=True)
        self.res5 = LResBlock_D(8 * n_feat, 16 * n_feat, downsample=False)
        self.fc = nn.utils.spectral_norm(nn.Linear(16 * n_feat, 1)).apply(init_weight)
        self.embedding = nn.Embedding(num_embeddings=1, embedding_dim=16 * n_feat).apply(init_weight)

    def forward(self, inputs, label, HINGE_LOSS=False):
        batch = inputs.size(0)  # (*,3,64,64)
        h = self.res1(inputs)  # ->(*,ch,32,32)
        h = self.attn(h)  # not change shape
        h = self.res2(h)  # ->(*,2ch,16,16)
        h = self.res3(h)  # ->(*,4ch,8,8)
        h = self.res4(h)  # ->(*,8ch,4,4)
        h = self.res5(h)  # ->(*,16ch,4,4)
        h = torch.sum((F.leaky_relu(h, 0.2)).view(batch, -1, 4 * 4), dim=2)  # GlobalSumPool ->(*,16ch)
        outputs = self.fc(h)  # ->(*,1)

        if label is not None:
            embed = self.embedding(label)  # ->(*,16ch)
            outputs += torch.sum(embed * h, dim=1, keepdim=True)  # ->(*,1)

        if not HINGE_LOSS:
            outputs = torch.sigmoid(outputs)

        return outputs


# BigGAN + scSEBlock
class ScSEDiscriminator(nn.Module):
    def __init__(self, config, common):
        super().__init__()
        n_feat = common.n_feat
        self.res1 = ScSEBlock_ResBlock_D(3, n_feat, downsample=True)
        self.scse1 = scSEBlock(n_feat, 32, 32)
        self.res2 = ScSEBlock_ResBlock_D(n_feat, 2 * n_feat, downsample=True)
        self.scse2 = scSEBlock(2 * n_feat, 16, 16)
        self.res3 = ScSEBlock_ResBlock_D(2 * n_feat, 4 * n_feat, downsample=True)
        self.scse3 = scSEBlock(4 * n_feat, 8, 8)
        self.res4 = ScSEBlock_ResBlock_D(4 * n_feat, 8 * n_feat, downsample=True)
        self.res5 = ScSEBlock_ResBlock_D(8 * n_feat, 16 * n_feat, downsample=False)
        self.fc = nn.utils.spectral_norm(nn.Linear(16 * n_feat, 1)).apply(init_weight)
        self.embedding = nn.Embedding(num_embeddings=1, embedding_dim=16 * n_feat).apply(init_weight)

    def forward(self, inputs, label, HINGE_LOSS=False):
        batch = inputs.size(0)  # (*,3,64,64)
        h = self.res1(inputs)  # ->(*,ch,32,32)
        h = self.scse1(h)  # not change shape

        h = self.res2(h)  # ->(*,2ch,16,16)
        h = self.scse2(h)  # not change shape

        h = self.res3(h)  # ->(*,4ch,8,8)
        h = self.scse3(h)  # not change shape

        h = self.res4(h)  # ->(*,8ch,4,4)
        h = self.res5(h)  # ->(*,16ch,4,4)
        h = torch.sum((F.relu(h)).view(batch, -1, 4 * 4), dim=2)  # GlobalSumPool ->(*,16ch)
        outputs = self.fc(h)  # ->(*,1)

        if label is not None:
            embed = self.embedding(label)  # ->(*,16ch)
            outputs += torch.sum(embed * h, dim=1, keepdim=True)  # ->(*,1)

        if not HINGE_LOSS:
            outputs = torch.sigmoid(outputs)

        return outputs


# BigGAN + leaky_relu + res concat
class LResConcatDiscriminator(nn.Module):
    def __init__(self, config, common):
        super().__init__()
        n_feat = common.n_feat
        self.sse = sSEBlock(3, 64, 64)
        self.res1 = LResConcat_ResBlock_D(3, n_feat, downsample=True)
        self.attn = Attention(2 * n_feat)
        self.res2 = LResConcat_ResBlock_D(2 * n_feat, n_feat, downsample=True)
        self.res3 = LResConcat_ResBlock_D(2 * n_feat, 2 * n_feat, downsample=True)
        self.res4 = LResConcat_ResBlock_D(4 * n_feat, 4 * n_feat, downsample=True)
        self.res5 = LResConcat_ResBlock_D(8 * n_feat, 8 * n_feat, downsample=False)
        self.fc = nn.utils.spectral_norm(nn.Linear(16 * n_feat, 1)).apply(init_weight)
        self.embedding = nn.Embedding(num_embeddings=1, embedding_dim=16 * n_feat).apply(init_weight)

    def forward(self, inputs, label, HINGE_LOSS=False):
        batch = inputs.size(0)  # (*,3,64,64)
        h = self.res1(inputs)  # ->(*,ch,32,32)
        h = self.attn(h)  # not change shape
        h = self.res2(h)  # ->(*,2ch,16,16)
        h = self.res3(h)  # ->(*,4ch,8,8)
        h = self.res4(h)  # ->(*,8ch,4,4)
        h = self.res5(h)  # ->(*,16ch,4,4)
        h = torch.sum((F.leaky_relu(h, 0.2)).view(batch, -1, 4 * 4), dim=2)  # GlobalSumPool ->(*,16ch)
        outputs = self.fc(h)  # ->(*,1)

        if label is not None:
            embed = self.embedding(label)  # ->(*,16ch)
            outputs += torch.sum(embed * h, dim=1, keepdim=True)  # ->(*,1)

        if not HINGE_LOSS:
            outputs = torch.sigmoid(outputs)

        return outputs


# BigGAN + leaky_relu + DenseBlock
class LDenseDiscriminator(nn.Module):
    def __init__(self, config, common):
        super().__init__()
        n_feat = common.n_feat
        self.res1 = DenseBlock_D(3, 128, n_feat)
        self.res1_2 = DenseBlock_D(3 + n_feat, 228, 32)
        self.tra1 = Transition_D(3 + n_feat + 32, 2 * n_feat)
        self.attn = Attention(2 * n_feat)
        self.res2 = DenseBlock_D(2 * n_feat, 128, 32)
        self.res2_2 = DenseBlock_D(2 * n_feat + 32, 128, 32)
        self.tra2 = Transition_D(2 * n_feat + 64, 4 * n_feat)
        self.res3 = DenseBlock_D(4 * n_feat, 128, 32)
        self.res3_2 = DenseBlock_D(4 * n_feat + 32, 128, 32)
        self.tra3 = Transition_D(4 * n_feat + 64, 8 * n_feat)
        self.res4 = DenseBlock_D(8 * n_feat, 128, 32)
        self.res4_2 = DenseBlock_D(8 * n_feat + 32, 128, 32)
        self.tra4 = Transition_D(8 * n_feat + 64, 16 * n_feat)
        self.res5 = DenseBlock_D(16 * n_feat, 128, 32)
        self.fc = nn.utils.spectral_norm(nn.Linear(16 * n_feat + 32, 1)).apply(init_weight)
        self.embedding = nn.Embedding(num_embeddings=1, embedding_dim=16 * n_feat + 32).apply(init_weight)

    def forward(self, inputs, label, HINGE_LOSS=False):
        batch = inputs.size(0)  # (*,3,64,64)
        h = self.tra1(self.res1_2(self.res1(inputs)))  # ->(*,ch,32,32)
        h = self.attn(h)  # not change shape
        h = self.tra2(self.res2_2(self.res2(h)))  # ->(*,2ch,16,16)
        h = self.tra3(self.res3_2(self.res3(h)))  # ->(*,4ch,8,8)
        h = self.tra4(self.res4_2(self.res4(h)))  # ->(*,8ch,4,4)
        h = self.res5(h)  # ->(*,16ch,4,4)
        h = torch.sum((F.leaky_relu(h, 0.2)).view(batch, -1, 4 * 4), dim=2)  # GlobalSumPool ->(*,16ch)
        outputs = self.fc(h)  # ->(*,1)

        if label is not None:
            embed = self.embedding(label)  # ->(*,16ch)
            outputs += torch.sum(embed * h, dim=1, keepdim=True)  # ->(*,1)

        if not HINGE_LOSS:
            outputs = torch.sigmoid(outputs)

        return outputs
