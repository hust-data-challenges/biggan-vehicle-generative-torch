from libs.models.modules.utils import *


# Define the Generator Network
# nn.ConvTranspose2d(params['ngf']*8, params['ngf']*4,
#             4, 2, 1, bias=False)
class Generator(nn.Module):
    def __init__(self, config, common):
        super().__init__()

        self.dense0 = nn.Linear(common.noise_dim,
                                common.image_width // config.scale_factor * common.image_height // config.scale_factor * 128)
        self.reshape1 = (common.image_height // config.scale_factor,
                         common.image_width // config.scale_factor, 128)
        self.transposed2 = TransposedConv(128, 512, 5, 1)
        self.dropout3 = nn.Dropout(config.dropout_rate)
        self.transposed4 = TransposedConv(512, 256, 5, 2, 1)
        self.dropout5 = nn.Dropout(config.dropout_rate)
        self.transposed6 = TransposedConv(256, 128, 5, 2, 1)
        self.transposed7 = TransposedConv(128, 64, 5, 2, 1)
        self.transposed8 = TransposedConv(64, 32, 5, 2, 1)
        self.dense9 = nn.Linear(32, 3)
        self.th10 = nn.Tanh()

    def forward(self, x):
        x = self.dense0(x).view(self.reshape1)
        x = self.transposed2(x)
        x = self.dropout3(x)
        x = self.transposed4(x)
        x = self.dropout5(x)
        x = self.transposed6(x)
        x = self.transposed7(x)
        x = self.transposed8(x)
        x = self.dense9(x)
        x = self.th10(x)
        return x

