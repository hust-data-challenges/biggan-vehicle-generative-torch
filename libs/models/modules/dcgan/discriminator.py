from libs.models.modules.utils import *


# Define the Discriminator Network
# in_channels, out_channels, kernel_size, stride,
#                  padding=0, bias=True, weight_initializer=None, dilation=0
# ConvSN in_channels, out_channels, kernel_size, stride, leaky_relu_slope
class Discriminator(nn.Module):
    def __init__(self, config, common):
        super().__init__()
        self.spectral_normalization = config.spectral_normalization
        if self.spectral_normalization:
            self.convsn2d0 = ConvSN2D(common.image_channels, 64, 5, 1, 'same', False, weight_initializer=None)
            self.leaky1 = nn.LeakyReLU(config.leaky_relu_slope)
            self.convsn2 = ConvSN(common.image_channels, 64, 5, 2, config.leaky_relu_slope)
            self.convsn3 = ConvSN(64, 128, 5, 2, config.leaky_relu_slope)
            self.convsn4 = ConvSN(128, 256, 5, 2, config.leaky_relu_slope)
            self.flatten5 = torch.flatten
            self.dense6 = DenseSN(256, 1)
            self.sigmodel7 = nn.Sigmoid()
        else:
            pass

    def forward(self, x):
        if self.spectral_normalization:
            x = self.convsn2d0(x)
            x = self.leaky1(x)
            x = self.convsn2(x)
            x = self.convsn3(x)
            x = self.convsn4(x)
            x = self.flatten5(x)
            x = self.dense6(x)
            x = self.sigmodel7(x)
        return x


if __name__ == '__main__':
    pass
