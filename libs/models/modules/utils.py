import math

import torch
import torch.nn as nn
import torch.nn.functional as F
from torch.nn.parameter import Parameter


def _l2normalize(v, eps=1e-12):
    return v / (torch.sum(v ** 2) ** 0.5 + eps)


def power_iteration(W, u):
    _u = u
    _v = _l2normalize(_u.matmul(W.t()))
    _u = _l2normalize(_v.matmul(W))
    return _u, _v


class DenseSN(nn.Module):

    def __init__(self, in_features, out_features, bias=True, weight_initializer=None):
        super(DenseSN, self).__init__()
        self.bias = bias
        self.weight_initializer = weight_initializer
        self.dense = nn.Linear(in_features, out_features, bias=bias)
        self.u = Parameter(torch.tensor(1, self.dense.weight.shape[-1]), requires_grad=False)
        self._reset_parameters()

    def _reset_parameters(self):
        if self.weight_initializer:
            self.weight = self.weight_initializer(self.weight)
        nn.init.normal_(self.u, 0, 1)

    def forward(self, inputs, training=None):
        w_shape = self.dense.weight.shape
        w_reshaped = self.dense.weight.view(-1, w_shape[-1])
        _u, _v = power_iteration(w_reshaped, self.u)
        # Calculate Sigma
        sigma = _v.matmul(w_reshaped)
        sigma = sigma.matmul(_u.t())
        # normalize it
        w_bar = w_reshaped / sigma
        # reshape weight tensor
        if training in {0, False}:
            w_bar = w_bar.view(w_shape)
        else:
            self.u.data = _u
            w_bar = w_bar.view(w_shape)
        self.dense.weight.data = w_bar
        outputs = self.dense(inputs)
        return outputs


# in, out, kernel_size, stride, padding='same', use_bias, weight_initializer
# in_channels, out_channels, kernel_size,
#                  stride=1, dilation=1, groups=1, bias=True
class ConvSN2D(nn.Module):

    def __init__(self, in_channels, out_channels, kernel_size, stride,
                 padding=0, bias=True, weight_initializer=None, dilation=0):
        super(ConvSN2D, self).__init__()
        if padding == 'same':
            self.conv = SamePaddingConv(in_channels, out_channels, kernel_size, stride, bias=bias)
        else:
            self.conv = nn.Conv2d(in_channels, out_channels, kernel_size, stride, padding, dilation, bias=bias)
        self.spectral_conv = nn.utils.spectral_norm(self.conv).apply(init_weight)


    def forward(self, inputs, training=None):
        outputs = self.spectral_conv(inputs)
        return outputs


class ConvSN2DTranspose(nn.Module):

    def __init__(self, in_channels, out_channels, kernel_size, stride, padding=0, output_padding=0,
                 dilation=None, bias=False, weight_initializer=None):
        super(ConvSN2DTranspose, self).__init__()
        self.weight_initializer = weight_initializer
        tconv = nn.ConvTranspose2d(in_channels, out_channels, kernel_size, stride, padding, output_padding,
                                        bias=bias, dilation=dilation)
        self.spectral_conv = nn.utils.spectral_norm(tconv).apply(init_weight)

    def forward(self, inputs, training=None):
        outputs = self.tconv(self.spectral_conv)
        return outputs


# nn.ConvTranspose2d(params['ngf']*8, params['ngf']*4,
#             4, 2, 1, bias=False)
class TransposedConv(nn.Module):

    def __init__(self, in_channels, out_channels, kernel_size, stride, padding=0, bias=False, weight_initializer=None):
        super(TransposedConv, self).__init__()
        self.weight_initializer = weight_initializer
        self.conv_transpose = nn.ConvTranspose2d(
            in_channels=in_channels,
            out_channels=out_channels,
            kernel_size=kernel_size,
            stride=stride,
            padding=padding,
            bias=bias
        )
        self.bn = nn.BatchNorm2d(out_channels)
        self.relu = nn.ReLU()
        self._reset_parameters()

    def _reset_parameters(self):
        if self.weight_initializer:
            self.weight_initializer(self.conv_transpose.weight)

    def forward(self, x):
        x = self.conv_transpose(x)
        x = self.bn(x)
        x = self.relu(x)
        return x


class TransposedConvSN(nn.Module):

    def __init__(self, in_channels, out_channels, kernel_size, stride, padding=0,
                 output_padding=None, dilation=None, bias=False, weight_initializer=None):
        super(TransposedConvSN, self).__init__()
        self.conv_transposeSN = ConvSN2DTranspose(in_channels, out_channels,
                                                  kernel_size, stride, padding, output_padding, dilation, bias,
                                                  weight_initializer)
        self.bn = nn.BatchNorm2d(out_channels)
        self.relu = nn.ReLU()

    def forward(self, x):
        x = self.conv_transposeSN(x)
        x = self.bn(x)
        x = self.relu(x)
        return x


# in_channels, out_channels, kernel_size, stride,
#                  padding=0, bias=True, weight_initializer=None, dilation=0
# self, in_channels, out_channels, kernel_size, stride,
#                  padding=0, bias=True, weight_initializer=None, dilation=0
class ConvSN(nn.Module):

    def __init__(self, in_channels, out_channels, kernel_size, stride, leaky_relu_slope=None,
                 bias=False, padding='same', weight_initializer=None, dilation=0):
        super(ConvSN, self).__init__()
        self.convsn2d = ConvSN2D(in_channels, out_channels, kernel_size, stride, padding,
                                 bias, weight_initializer, dilation)
        self.bn = nn.BatchNorm2d(out_channels)
        self.leaky = nn.LeakyReLU(leaky_relu_slope)

    def forward(self, x):
        x = self.convsn2d(x)
        x = self.bn(x)
        x = self.leaky(x)
        return x


class Conv(nn.Module):

    def __init__(self, in_channels, out_channels, kernel_size, stride, padding=0,
                 dilation=0, bias=False, leaky_relu_slope=None, weight_initializer=None):
        super(Conv, self).__init__()
        self.weight_initializer = weight_initializer
        self.conv2d = nn.Conv2d(in_channels, out_channels, kernel_size, stride, padding, dilation, bias=bias)
        self.bn = nn.BatchNorm2d(out_channels)
        self.leaky = nn.LeakyReLU(leaky_relu_slope)
        self._reset_parameters()

    def _reset_parameters(self):
        if self.weight_initializer:
            self.weight_initializer(self.conv2d.weight)

    def forward(self, x):
        x = self.convsn2d(x)
        x = self.bn(x)
        x = self.leaky(x)
        return x


class SamePaddingConv(nn.Conv2d):

    def __init__(self, in_channels, out_channels, kernel_size,
                 stride=1, dilation=1, groups=1, bias=True):
        super(SamePaddingConv, self).__init__(in_channels, out_channels, kernel_size,
                                              stride, 0, dilation, groups, bias)
        self.stride = self.stride if len(self.stride) == 2 else [self.stride[0]] * 2

    def forward(self, x):
        ih, iw = x.size()[-2:]
        kh, kw = self.weight.size()[-2:]
        sh, sw = self.stride
        oh, ow = math.ceil(ih / sh), math.ceil(iw / sw)
        pad_h = max((oh - 1) * self.stride[0] + (kh - 1) * self.dilation[0] + 1 - ih, 0)
        pad_w = max((ow - 1) * self.stride[1] + (kw - 1) * self.dilation[1] + 1 - iw, 0)
        if pad_h > 0 or pad_w > 0:
            x = F.pad(x, [pad_w // 2, pad_w - pad_w // 2, pad_h // 2, pad_h - pad_h // 2])
        return F.conv2d(x, self.weight, self.bias, self.stride, self.padding, self.dilation, self.groups)


class Conv2dStaticSamePadding(nn.Conv2d):
    """ 2D Convolutions like TensorFlow, for a fixed image size"""

    def __init__(self, in_channels, out_channels, kernel_size, image_size=None, **kwargs):
        super().__init__(in_channels, out_channels, kernel_size, **kwargs)
        self.stride = self.stride if len(self.stride) == 2 else [self.stride[0]] * 2

        # Calculate padding based on image size and save it
        assert image_size is not None
        ih, iw = image_size if type(image_size) == list else [image_size, image_size]
        kh, kw = self.weight.size()[-2:]
        sh, sw = self.stride
        oh, ow = math.ceil(ih / sh), math.ceil(iw / sw)
        pad_h = max((oh - 1) * self.stride[0] + (kh - 1) * self.dilation[0] + 1 - ih, 0)
        pad_w = max((ow - 1) * self.stride[1] + (kw - 1) * self.dilation[1] + 1 - iw, 0)
        if pad_h > 0 or pad_w > 0:
            self.static_padding = nn.ZeroPad2d((pad_w // 2, pad_w - pad_w // 2, pad_h // 2, pad_h - pad_h // 2))
        else:
            self.static_padding = Identity()

    def forward(self, x):
        x = self.static_padding(x)
        x = F.conv2d(x, self.weight, self.bias, self.stride, self.padding, self.dilation, self.groups)
        return x


class Identity(nn.Module):
    def __init__(self, ):
        super(Identity, self).__init__()

    def forward(self, inputs):
        return inputs


def count_parameters(model):
    return sum(p.numel() for p in model.parameters() if p.requires_grad)


def conv3x3(in_channel, out_channel):  # not change resolusion
    return nn.Conv2d(in_channel, out_channel,
                     kernel_size=3, stride=1, padding=1, dilation=1, bias=False)


def conv1x1(in_channel, out_channel):  # not change resolution
    return nn.Conv2d(in_channel, out_channel,
                     kernel_size=1, stride=1, padding=0, dilation=1, bias=False)


def init_weight(m):
    classname = m.__class__.__name__
    if classname.find('Conv') != -1:
        nn.init.orthogonal_(m.weight, gain=1)
        if m.bias is not None:
            m.bias.data.zero_()

    elif classname.find('Batch') != -1:
        m.weight.data.normal_(1, 0.02)
        m.bias.data.zero_()

    elif classname.find('Linear') != -1:
        nn.init.orthogonal_(m.weight, gain=1)
        if m.bias is not None:
            m.bias.data.zero_()

    elif classname.find('Embedding') != -1:
        nn.init.orthogonal_(m.weight, gain=1)


class cSEBlock(nn.Module):
    def __init__(self, c, feat):
        super().__init__()
        self.attention_fc = nn.Linear(feat, 1, bias=False)
        self.bias = nn.Parameter(torch.zeros(1, c, 1), requires_grad=True)
        self.sigmoid = nn.Sigmoid()
        self.dropout = nn.Dropout2d(0.1)

    def forward(self, inputs):
        batch, c, h, w = inputs.size()
        x = inputs.view(batch, c, -1)
        x = self.attention_fc(x) + self.bias
        x = x.view(batch, c, 1, 1)
        x = self.sigmoid(x)
        x = self.dropout(x)
        return inputs * x


class sSEBlock(nn.Module):
    def __init__(self, c, h, w):
        super().__init__()
        self.attention_fc = nn.Linear(c, 1, bias=False).apply(init_weight)
        self.bias = nn.Parameter(torch.zeros(1, h, w, 1), requires_grad=True)
        self.sigmoid = nn.Sigmoid()

    def forward(self, inputs):
        # batch, c, h, w = inputs.size()
        x = torch.transpose(inputs, 1, 2)  # (*,c,h,w)->(*,h,c,w)
        x = torch.transpose(x, 2, 3)  # (*,h,c,w)->(*,h,w,c)
        x = self.attention_fc(x) + self.bias
        x = torch.transpose(x, 2, 3)  # (*,h,w,1)->(*,h,1,w)
        x = torch.transpose(x, 1, 2)  # (*,h,1,w)->(*,1,h,w)
        x = self.sigmoid(x)
        return inputs * x


class scSEBlock(nn.Module):
    def __init__(self, c, h, w):
        super().__init__()
        self.cSE = cSEBlock(c, h * w)
        self.sSE = sSEBlock(c, h, w)

    def forward(self, inputs):
        x1 = self.cSE(inputs)
        x2 = self.sSE(inputs)
        return x1 + x2


class Attention(nn.Module):
    def __init__(self, channels):
        super().__init__()
        self.channels = channels
        self.theta = nn.utils.spectral_norm(conv1x1(channels, channels // 8)).apply(init_weight)
        self.phi = nn.utils.spectral_norm(conv1x1(channels, channels // 8)).apply(init_weight)
        self.g = nn.utils.spectral_norm(conv1x1(channels, channels // 2)).apply(init_weight)
        self.o = nn.utils.spectral_norm(conv1x1(channels // 2, channels)).apply(init_weight)
        self.gamma = nn.Parameter(torch.tensor(0.), requires_grad=True)

    def forward(self, inputs):
        batch, c, h, w = inputs.size()
        theta = self.theta(inputs)  # ->(*,c/8,h,w)
        phi = F.max_pool2d(self.phi(inputs), [2, 2])  # ->(*,c/8,h/2,w/2)
        g = F.max_pool2d(self.g(inputs), [2, 2])  # ->(*,c/2,h/2,w/2)

        theta = theta.view(batch, self.channels // 8, -1)  # ->(*,c/8,h*w)
        phi = phi.view(batch, self.channels // 8, -1)  # ->(*,c/8,h*w/4)
        g = g.view(batch, self.channels // 2, -1)  # ->(*,c/2,h*w/4)

        beta = F.softmax(torch.bmm(theta.transpose(1, 2), phi), -1)  # ->(*,h*w,h*w/4)
        o = self.o(torch.bmm(g, beta.transpose(1, 2)).view(batch, self.channels // 2, h, w))  # ->(*,c,h,w)
        return self.gamma * o + inputs


class ConditionalNorm(nn.Module):
    def __init__(self, in_channel, n_condition):
        super().__init__()
        self.bn = nn.BatchNorm2d(in_channel, affine=False)  # no learning parameters
        self.embed = nn.Linear(n_condition, in_channel * 2)

        nn.init.orthogonal_(self.embed.weight.data[:, :in_channel], gain=1)
        self.embed.weight.data[:, in_channel:].zero_()

    def forward(self, inputs, label):
        out = self.bn(inputs)
        embed = self.embed(label.float())
        gamma, beta = embed.chunk(2, dim=1)
        gamma = gamma.unsqueeze(2).unsqueeze(3)
        beta = beta.unsqueeze(2).unsqueeze(3)
        out = gamma * out + beta
        return out


# #BigGAN + scSEBlock
class ScSEBlock_ResBlock_G(nn.Module):
    def __init__(self, in_channel, out_channel, condition_dim, upsample=True):
        super().__init__()
        self.cbn1 = ConditionalNorm(in_channel, condition_dim)
        self.upsample = nn.Sequential()
        if upsample:
            self.upsample.add_module('upsample', nn.Upsample(scale_factor=2, mode='nearest'))
        self.conv3x3_1 = nn.utils.spectral_norm(conv3x3(in_channel, out_channel)).apply(init_weight)
        self.cbn2 = ConditionalNorm(out_channel, condition_dim)
        self.conv3x3_2 = nn.utils.spectral_norm(conv3x3(out_channel, out_channel)).apply(init_weight)
        self.conv1x1 = nn.utils.spectral_norm(conv1x1(in_channel, out_channel)).apply(init_weight)

    def forward(self, inputs, condition):
        x = F.relu(self.cbn1(inputs, condition))
        x = self.upsample(x)
        x = self.conv3x3_1(x)
        x = self.conv3x3_2(F.relu(self.cbn2(x, condition)))
        x += self.conv1x1(self.upsample(inputs))  # shortcut
        return x


class ScSEBlock_ResBlock_D(nn.Module):
    def __init__(self, in_channel, out_channel, downsample=True):
        super().__init__()
        self.layer = nn.Sequential(
            nn.ReLU(),
            nn.utils.spectral_norm(conv3x3(in_channel, out_channel)).apply(init_weight),
            nn.ReLU(),
            nn.utils.spectral_norm(conv3x3(out_channel, out_channel)).apply(init_weight),
        )
        self.shortcut = nn.Sequential(
            nn.utils.spectral_norm(conv1x1(in_channel, out_channel)).apply(init_weight),
        )
        if downsample:
            self.layer.add_module('avgpool', nn.AvgPool2d(kernel_size=2, stride=2))
            self.shortcut.add_module('avgpool', nn.AvgPool2d(kernel_size=2, stride=2))

    def forward(self, inputs):
        x = self.layer(inputs)
        x += self.shortcut(inputs)
        return x


# ===================================================

# BigGAN + leaky_relu
class LResBlock_G(nn.Module):
    def __init__(self, in_channel, out_channel, condition_dim, upsample=True):
        super().__init__()
        self.cbn1 = ConditionalNorm(in_channel, condition_dim)
        self.upsample = nn.Sequential()
        if upsample:
            self.upsample.add_module('upsample', nn.Upsample(scale_factor=2, mode='nearest'))
        self.conv3x3_1 = nn.utils.spectral_norm(conv3x3(in_channel, out_channel)).apply(init_weight)
        self.cbn2 = ConditionalNorm(out_channel, condition_dim)
        self.conv3x3_2 = nn.utils.spectral_norm(conv3x3(out_channel, out_channel)).apply(init_weight)
        self.conv1x1 = nn.utils.spectral_norm(conv1x1(in_channel, out_channel)).apply(init_weight)

    def forward(self, inputs, condition):
        x = F.leaky_relu(self.cbn1(inputs, condition))
        x = self.upsample(x)
        x = self.conv3x3_1(x)
        x = self.conv3x3_2(F.leaky_relu(self.cbn2(x, condition)))
        x += self.conv1x1(self.upsample(inputs))  # shortcut
        return x


class LResBlock_D(nn.Module):
    def __init__(self, in_channel, out_channel, downsample=True):
        super().__init__()
        self.layer = nn.Sequential(
            nn.LeakyReLU(0.2),
            nn.utils.spectral_norm(conv3x3(in_channel, out_channel)).apply(init_weight),
            nn.LeakyReLU(0.2),
            nn.utils.spectral_norm(conv3x3(out_channel, out_channel)).apply(init_weight),
        )
        self.shortcut = nn.Sequential(
            nn.utils.spectral_norm(conv1x1(in_channel, out_channel)).apply(init_weight),
        )
        if downsample:
            self.layer.add_module('avgpool', nn.AvgPool2d(kernel_size=2, stride=2))
            self.shortcut.add_module('avgpool', nn.AvgPool2d(kernel_size=2, stride=2))

    def forward(self, inputs):
        x = self.layer(inputs)
        x += self.shortcut(inputs)
        return x


# ==============================
# BigGAN + leaky_relu + res concat
class LResConcat_ResBlock_G(nn.Module):
    def __init__(self, in_channel, out_channel, condition_dim, upsample=True):
        super().__init__()
        self.cbn1 = ConditionalNorm(in_channel, condition_dim)
        self.upsample = nn.Sequential()
        if upsample:
            self.upsample.add_module('upsample', nn.Upsample(scale_factor=2, mode='nearest'))
        self.conv3x3_1 = nn.utils.spectral_norm(conv3x3(in_channel, out_channel)).apply(init_weight)
        self.cbn2 = ConditionalNorm(out_channel, condition_dim)
        self.conv3x3_2 = nn.utils.spectral_norm(conv3x3(out_channel, out_channel)).apply(init_weight)
        self.conv1x1 = nn.utils.spectral_norm(conv1x1(in_channel, out_channel)).apply(init_weight)

    def forward(self, inputs, condition):
        x = F.leaky_relu(self.cbn1(inputs, condition))
        x = self.upsample(x)
        x = self.conv3x3_1(x)
        x = self.conv3x3_2(F.leaky_relu(self.cbn2(x, condition)))
        x1 = self.conv1x1(self.upsample(inputs))  # shortcut
        x = torch.cat([x, x1], dim=1)
        return x


class LResConcat_ResBlock_D(nn.Module):
    def __init__(self, in_channel, out_channel, downsample=True):
        super().__init__()
        self.layer = nn.Sequential(
            nn.LeakyReLU(0.2),
            nn.utils.spectral_norm(conv3x3(in_channel, out_channel)).apply(init_weight),
            nn.LeakyReLU(0.2),
            nn.utils.spectral_norm(conv3x3(out_channel, out_channel)).apply(init_weight),
        )
        self.shortcut = nn.Sequential(
            nn.utils.spectral_norm(conv1x1(in_channel, out_channel)).apply(init_weight),
        )
        if downsample:
            self.layer.add_module('avgpool', nn.AvgPool2d(kernel_size=2, stride=2))
            self.shortcut.add_module('avgpool', nn.AvgPool2d(kernel_size=2, stride=2))

    def forward(self, inputs):
        x = self.layer(inputs)
        x1 = self.shortcut(inputs)
        x = torch.cat([x, x1], dim=1)
        return x


# ==============================
class DenseBlock_G(nn.Module):
    def __init__(self, in_channel, med_channel=128, growth_rate=32,
                 condition_dim=None, upsample=True):
        super().__init__()
        self.cbn1 = ConditionalNorm(in_channel, condition_dim)
        self.conv1x1 = nn.utils.spectral_norm(conv1x1(in_channel, med_channel)).apply(init_weight)
        self.cbn2 = ConditionalNorm(med_channel, condition_dim)
        self.conv3x3 = nn.utils.spectral_norm(conv3x3(med_channel, growth_rate)).apply(init_weight)

    def forward(self, inputs, condition):
        x = self.conv1x1(F.leaky_relu(self.cbn1(inputs, condition)))
        x = self.conv3x3(F.leaky_relu(self.cbn2(x, condition)))
        x = torch.cat([inputs, x], dim=1)
        return x


class Transition_G(nn.Module):
    def __init__(self, in_channel, out_channel):
        super().__init__()
        self.upsample = nn.Upsample(scale_factor=2, mode='nearest')
        self.conv1x1 = nn.utils.spectral_norm(conv1x1(in_channel, out_channel)).apply(init_weight)

    def forward(self, inputs):
        x = self.upsample(inputs)
        x = self.conv1x1(x)
        return x


class DenseBlock_D(nn.Module):
    def __init__(self, in_channel, med_channel=128, growth_rate=32,
                 downsample=True):
        super().__init__()
        self.conv1x1 = nn.utils.spectral_norm(conv1x1(in_channel, med_channel)).apply(init_weight)
        self.conv3x3 = nn.utils.spectral_norm(conv3x3(med_channel, growth_rate)).apply(init_weight)

    def forward(self, inputs):
        x = self.conv1x1(F.leaky_relu(inputs, 0.2))
        x = self.conv3x3(F.leaky_relu(x, 0.2))
        x = torch.cat([inputs, x], dim=1)
        return x


class Transition_D(nn.Module):
    def __init__(self, in_channel, out_channel):
        super().__init__()
        self.conv1x1 = nn.utils.spectral_norm(conv1x1(in_channel, out_channel)).apply(init_weight)
        self.downsample = nn.AvgPool2d(kernel_size=2, stride=2)

    def forward(self, inputs):
        x = self.conv1x1(inputs)
        x = self.downsample(x)
        return x
