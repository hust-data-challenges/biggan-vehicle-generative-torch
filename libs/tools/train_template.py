from libs.tools import dcgan_train, biggan_train

training_mapper = {
    'dcgan': dcgan_train.train,
    'BigGAN': biggan_train.train
}