import hashlib
from scipy.misc import imread, imresize, imshow
import matplotlib.pyplot as plt
import matplotlib.gridspec as gridspec
import time
import numpy as np
import os
import hashlib, os

file_list = os.listdir('./storage/processed_data')

duplicates = []
hash_keys = dict()
for index, filename in  enumerate(os.listdir('./storage/processed_data/')):  #listdir('.') = current directory
    path = './storage/processed_data/' + filename
    if os.path.isfile(path):
        with open(path, 'rb') as f:
            filehash = hashlib.md5(f.read()).hexdigest()
        if filehash not in hash_keys:
            hash_keys[filehash] = index
        else:
            duplicates.append((index,hash_keys[filehash]))

for index in duplicates:
    os.remove('./storage/processed_data/' + file_list[index[0]])