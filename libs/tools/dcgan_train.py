from torch import optim

from libs.optimizers.optimizer_factory import get_generator_optimizer, get_discriminator_optimizer
from libs.utils.common import make_all_dirs
from libs.models.model_factory import get_discriminator, get_generator
import torch
from libs.utils.logger import Logger
from libs.utils.timer import Timer
from libs.models.model_factory import get_discriminator, get_generator
from libs.transformer import get_transform
from libs.datasets.dataset_factory import get_dataloader
import torch.backends.cudnn as cudnn
import torch.nn as nn
from libs.utils.training_utils import count_parameters
device = torch.device("cuda:0" if torch.cuda.is_available() else "cpu")


def train(config, params_config, g_resume_checkpoint, d_resume_checkpoint, n_epochs, df, print_model=False):
    # preparation
    make_all_dirs(config.common)
    transform1, transform2 = get_transform(config.transform, config.common)
    preprocessor = None
    data_loader = get_dataloader(config, preprocessor, transform1, transform2)
    print("============ Building Model =============")
    generator = get_generator(config.model, config.common)
    discriminator = get_discriminator(config.model, config.common)
    
    use_gpu = torch.cuda.is_available()
    if use_gpu:
        print('Utilize GPUs for computation')
        print('Number of GPU available', torch.cuda.device_count())
        generator = generator.to(device)
        discriminator = discriminator.to(device)
        cudnn.benchmark = True
        if torch.cuda.device_count() > 1:
            generator = torch.nn.DataParallel(generator)
            discriminator = torch.nn.DataParallel(discriminator)

    count_parameters(generator)
    count_parameters(discriminator)

    dis_criterion = nn.BCELoss().to(device)
    optimizerG = get_generator_optimizer(config, generator.parameters())
    optimizerD = get_discriminator_optimizer(config, discriminator.parameters())

    real_image = 0.9
    fake_image = 0
    D_losses = []
    G_losses = []
