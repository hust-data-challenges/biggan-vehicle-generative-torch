import shutil
import time

import numpy as np
import torch
from torch.backends import cudnn
from torchvision.transforms import transforms

from libs.datasets.dataset_factory import get_dataloader
from libs.evaluation import MiFIDEvaluator
from libs.models.model_factory import get_generator, get_discriminator
from libs.optimizers.optimizer_factory import get_generator_optimizer, get_discriminator_optimizer
from libs.scheduler_factory import get_scheduler
from libs.utils.config_parser import cfg_from_file
import os
from libs.utils.training_utils import count_parameters
import torch.nn.functional as F
import torch.nn as nn

from libs.utils.logger import Logger
import matplotlib.pyplot as plt
from libs.utils.timer import Timer

device = torch.device("cuda" if torch.cuda.is_available() else "cpu")


def elapsed_time(start_time):
    return time.time() - start_time


def write_logs(logger, title, value, epoch):
    logger.scalar_summary(title, value, step=epoch)


def gen_outputs(evaluator, epoch, gen_imgs_dir, fid_logger, distance_logger, mifid_logger):
    make_dir(gen_imgs_dir)
    fid_value, distance, mi_fid_score = evaluator.evaluate(gen_imgs_dir)
    write_logs(fid_logger, 'fid score', fid_value, epoch)
    write_logs(distance_logger, 'distance score', distance, epoch)
    write_logs(mifid_logger, 'mi fid score', mi_fid_score, epoch)
    print(f'FID: {fid_value:.5f}')
    print(f'distance: {distance:.5f}')
    print(f'MiFID: {mi_fid_score:.5f}')
    shutil.rmtree(gen_imgs_dir)
    print('Removed temporary image directory.')
    print('Final epoch.')


if torch.cuda.is_available():
    cudnn.benchmark = True


# https://www.kaggle.com/osciiart/resnet34-mel-ver3-log-multi-hardaug?scriptVersionId=13887036
def cycle(iterable):
    """
    dataloaderをiteratorに変換
    :param iterable:
    :return:
    """
    while True:
        for x in iterable:
            yield x


def make_dir(path):
    os.makedirs(path, exist_ok=True)


def load_previous(checkpoint_log):
    if not os.path.exists(checkpoint_log):
        return False
    with open(checkpoint_log, 'r') as f:
        lines = f.readlines()
    epochs, g_resume_checkpoints, d_resume_checkpoints = [list() for _ in range(3)]
    for line in lines:
        line_list = line.replace(':', '').replace('---', '').strip('\n').split(' ')
        epochs.append(int(line_list[1]))
        g_resume_checkpoints.append(line_list[2])
        d_resume_checkpoints.append(line_list[3])
    return epochs, g_resume_checkpoints, d_resume_checkpoints


def resume_checkpoint(netG, netD, g_checkpoint, d_checkpoint, start_epoch):
    if not os.path.isfile(g_checkpoint) \
            or not os.path.isfile(d_checkpoint):
        print(f" => no checkpoint found at '{g_checkpoint}' or '{d_checkpoint}'")
        return False
    netG.load_state_dict(torch.load(g_checkpoint))
    netD.load_state_dict(torch.load(d_checkpoint))

    return netG, netD, start_epoch


def load_checkpoint(netG, netD, g_checkpoint, d_checkpoint, checkpoint_log):
    start_epoch = 0
    if g_checkpoint is None or d_checkpoint is None:
        previous = load_previous(checkpoint_log)
        if previous:
            start_epoch = previous[0][-1]
            netG, netD = resume_checkpoint(netG, netD, previous[1][-1], previous[2][-1], start_epoch)
    else:
        netG, netD = resume_checkpoint(netG, netD, g_checkpoint, d_checkpoint, start_epoch)

    return netG, netD, start_epoch


def generate_img(netG, fixed_noise, fixed_aux_labels=None):
    if fixed_aux_labels is not None:
        gen_image = netG(fixed_noise, fixed_aux_labels).to('cpu').clone().detach().squeeze(0)
    else:
        gen_image = netG(fixed_noise).to('cpu').clone().detach().squeeze(0)
    # denormalize
    gen_image = gen_image * 0.5 + 0.5
    gen_image_numpy = gen_image.numpy().transpose(0, 2, 3, 1)
    return gen_image_numpy


def show_generate_imgs(netG, fixed_noise, fixed_aux_labels=None, epoch=-1):
    gen_images_numpy = generate_img(netG, fixed_noise, fixed_aux_labels)

    fig = plt.figure(figsize=(25, 16))
    # display 10 images from each class
    for i, img in enumerate(gen_images_numpy):
        ax = fig.add_subplot(4, 8, i + 1, xticks=[], yticks=[])
        plt.imshow(img)
    plt.savefig("./experiments/pngs/" + "Generated_image_at_" + str(epoch) + ".png")
    # plt.show()
    # plt.close()


def make_dirs(config, training_config):
    model_dir = config.experiment_dir + 'exp_dirs/' + training_config.model + "/" + training_config.version + '/'
    log_dir = model_dir + 'logs/'
    log_mifids = model_dir + 'mifid_logs/'
    final_images = model_dir + 'image_results/'
    temporal_images = model_dir + 'temporal_images/'  # save epoch_{}.png
    checkpoints = model_dir + 'checkpoints/'  # save checkpoint_epoch_g_loss_d_loss_FID_epoch_{}.pth and checkpoint_epoch_g_loss_d_loss_FID_epoch_{}.pth
    # pretrained_logs.txt for each model_dir
    # experiments/summaries_training.csv
    # experiments/NOTE_AND_TODO.txt
    creating_dirs = [model_dir, log_dir, log_mifids, final_images, temporal_images, checkpoints]
    for dir in creating_dirs:
        make_dir(dir)
    print("Create all of training dirs successfully !!")
    return model_dir, log_dir, log_mifids, final_images, temporal_images, checkpoints


def run(config_file, training_config_file, summaries_file, g_checkpoint=None, d_checkpoint=None, print_model=False):
    config = cfg_from_file(config_file)
    training_config = cfg_from_file(training_config_file)
    # common preparation
    model_dir, log_dir, log_mifids, final_images, temporal_images, checkpoints = make_dirs(config, training_config)
    f = open(summaries_file, 'w')
    # ==================

    # build dataloader
    transform1 = transforms.Compose([transforms.Resize(training_config.img_size)])
    transform2 = transforms.Compose([transforms.RandomCrop(training_config.img_size),
                                     transforms.RandomHorizontalFlip(p=0.5),
                                     transforms.ToTensor(),
                                     transforms.Normalize(mean=[config.MEAN1, config.MEAN2, config.MEAN3],

                                                          std=[config.STD1, config.STD1, config.STD1])])
    # dataloader
    dataloader = get_dataloader(config, training_config, transform1, transform2)

    # model
    netG = get_generator(config, training_config).to(device)
    netD = get_discriminator(config, training_config).to(device)
    if training_config.EMA:
        # EMA of G for sampling
        netG_EMA = get_generator(config, training_config, True).to(device)
        netG_EMA.load_state_dict(netG.state_dict())
        for p in netG_EMA.parameters():
            p.requires_grad = False

    if torch.cuda.device_count() > 1:
        netG = torch.nn.DataParallel(netG)
        netD = torch.nn.DataParallel(netD)

    print(count_parameters(netG))
    print(count_parameters(netD))

    if print_model:
        print(netG.summary())
        print(netD.summary())

    # optimizer
    optimizerG = get_generator_optimizer(config.optimizer, netG.parameters())
    optimizerD = get_discriminator_optimizer(config.optimizer, netD.parameters())

    # lr_scheduler
    schedulerG = get_scheduler(training_config.scheduler.generator, optimizerG, 0)
    schedulerD = get_scheduler(training_config.scheduler.discriminator, optimizerD, 0)

    # metrics
    # criterion = get_losser(training_config.loss, config)

    # evaluator
    evaluator = MiFIDEvaluator(config.common.evaluation_model, config.common.evaluation_pretrain)

    # logger
    g_logger = Logger(log_dir + 'generation_logs')
    d_logger = Logger(log_dir + 'discrimination_logs')
    fid_logger = Logger(log_mifids + 'fid_logs')
    distance_logger = Logger(log_mifids + 'distance_logs')
    mifid_logger = Logger(log_mifids + 'mifid_logs')

    # load pretrained
    load_checkpoint(netG, netD, g_checkpoint, d_checkpoint, model_dir + 'pretrained_logs.txt')

    n_classes = 1
    real_label = 0.9
    fake_label = 0

    fixed_noise = torch.randn(32, training_config.noise_dim, 1, 1, device=device)
    fixed_aux_labels = np.random.randint(0, n_classes, 32)
    fixed_aux_labels_ohe = np.eye(n_classes)[fixed_aux_labels]
    fixed_aux_labels_ohe = torch.from_numpy(fixed_aux_labels_ohe[:, :, np.newaxis, np.newaxis])
    fixed_aux_labels_ohe = fixed_aux_labels_ohe.float().to(device, non_blocking=True)

    netG.train()
    netD.train()

    G_loss_list = []
    D_loss_list = []
    all_gl = np.array([])
    all_dl = np.array([])

    for epoch in range(1, training_config.epochs + 1):
        # if elapsed_time(start_time) > TIME_LIMIT:
        #     print(f'elapsed_time go beyond {TIME_LIMIT} sec')
        #     break
        g_losses = []
        d_losses = []

        if training_config.milestones:
            print('lrG = ', schedulerG.get_lr()[0])
            print('lrD = ', schedulerD.get_lr()[0])

        D_running_loss = 0
        G_running_loss = 0
        start_time = time.time()
        for idx, data in enumerate(dataloader):
            # update D network
            real_images = data[0].to(device, non_blocking=True)
            aux_labels = data[1].long().to(device, non_blocking=True)
            batch_size = real_images.size(0)

            if training_config.label_noise:
                real_label = 0.9
                fake_label = 0
                if np.random.random() < training_config.label_noise_prob:
                    real_label = 0
                    fake_label = 0.9

            for _ in range(training_config.n_iter_D):
                # training with real image
                netD.zero_grad()
                dis_labels = torch.full((batch_size, n_classes), real_label, device=device)
                dis_output = netD(real_images, aux_labels)
                if training_config.loss == 'hinge_loss':
                    errD_real = torch.mean(F.relu(1 - dis_output))
                else:
                    errD_real = nn.BCELoss().to(device)(dis_output, dis_labels)
                errD_real.backward(retain_graph=True)

                # train with fake
                noise = torch.randn(batch_size, training_config.noise_dim, 1, 1, device=device)
                # noise = noise / noise.norm(dim=1, keepdim=True)
                aux_labels = np.random.randint(0, n_classes, batch_size)
                aux_labels_ohe = np.eye(n_classes)[aux_labels]
                aux_labels_ohe = torch.from_numpy(aux_labels_ohe[:, :, np.newaxis, np.newaxis])
                aux_labels_ohe = aux_labels_ohe.float().to(device, non_blocking=True)
                aux_labels = torch.from_numpy(aux_labels).long().to(device, non_blocking=True)

                # print('noise ', noise.size())
                # print('aux_labels_ohe.size', aux_labels_ohe.size())
                # noise  torch.Size([32, 60, 1, 1])
                # aux_labels_ohe.size torch.Size([32, 1, 1, 1])
                # torch.Size([32, 120, 1, 1]) torch.Size([32, 1, 1, 1]) truth

                fake = netG(noise, aux_labels_ohe)  # output shape=(*,3,64,64)
                dis_labels.fill_(fake_label)
                dis_output = netD(fake.detach(), aux_labels)
                if training_config.loss == 'hinge_loss':
                    errD_fake = torch.mean(F.relu(1 + dis_output))
                else:
                    errD_fake = nn.BCELoss().to(device)(dis_output, dis_labels)
                errD_fake.backward(retain_graph=True)
                D_running_loss += (errD_real.item() + errD_fake.item()) / len(dataloader)
                optimizerD.step()

            ############################
            # (2) Update G network
            ###########################
            netG.zero_grad()
            dis_labels.fill_(real_label)  # fake labels are real for generator cost
            noise = torch.randn(training_config.batch_size, training_config.noise_dim, 1, 1, device=device)
            aux_labels = np.random.randint(0, n_classes, training_config.batch_size)
            aux_labels_ohe = np.eye(n_classes)[aux_labels]
            aux_labels_ohe = torch.from_numpy(aux_labels_ohe[:, :, np.newaxis, np.newaxis])
            aux_labels_ohe = aux_labels_ohe.float().to(device, non_blocking=True)
            aux_labels = torch.from_numpy(aux_labels).long().to(device, non_blocking=True)
            fake = netG(noise, aux_labels_ohe)

            dis_output = netD(fake, aux_labels)
            if training_config.loss == 'hinge_loss':
                errG = - torch.mean(dis_output)
            else:
                errG = nn.BCELoss().to(device)(dis_output, dis_labels)
            errG.backward(retain_graph=True)
            G_running_loss += errG.item() / len(dataloader)
            optimizerG.step()

            g_losses.append(G_running_loss)
            d_losses.append(D_running_loss / training_config.n_iter_D)
            all_gl = np.append(all_gl, np.array([g_losses]))
            all_dl = np.append(all_dl, np.array([d_losses]))

        if training_config.milestones:
            schedulerG.step()
            schedulerD.step()

        if config.ema:
            # update netG_EMA
            param_itr = cycle(netG.parameters())
            for i, p_EMA in enumerate(netG_EMA.parameters()):
                p = next(param_itr)
                p_EMA.data = (1 - config.ema.decay_rate) * p_EMA.data + config.ema.decay_rate * p.data
                p_EMA.requires_grad = False
        # log
        D_loss_list.append(D_running_loss)
        G_loss_list.append(G_running_loss)

        write_logs(g_logger, 'generative_loss', sum(g_losses) / len(g_losses), epoch)
        write_logs(d_logger, 'discriminative_loss', sum(d_losses) / len(d_losses), epoch)

        # output
        if epoch % config.output_freq == 0:
            print(
                '[{:d}/{:d}] D_loss = {:.3f}, G_loss = {:.3f}, elapsed_time = {:.1f} min'.format(epoch,
                                                                                                 training_config.epochs,
                                                                                                 D_running_loss,
                                                                                                 G_running_loss,
                                                                                                 elapsed_time(
                                                                                                     start_time) / 60))

        if epoch in config.show_epoch_list:
            print('epoch = {}'.format(epoch))
            if not config.ema:
                show_generate_imgs(netG, fixed_noise, fixed_aux_labels_ohe)
            elif config.ema:
                show_generate_imgs(netG_EMA, fixed_noise, fixed_aux_labels_ohe)

        if epoch % training_config.gen_cycle:
            gen_outputs(evaluator, epoch, temporal_images, fid_logger, distance_logger, mifid_logger)

        if epoch % 100 == 0:
            if not config.ema:
                torch.save(netG.state_dict(), f'generator_epoch{epoch}.pth')
            elif config.ema:
                torch.save(netG_EMA.state_dict(), f'generator_epoch{epoch}.pth')

    if not config.ema:
        torch.save(netG.state_dict(), 'generator.pth')
    elif config.ema:
        torch.save(netG_EMA.state_dict(), 'generator.pth')
    torch.save(netD.state_dict(), 'discriminator.pth')

    res = {'netG': netG,
           'netD': netD,
           'nz': training_config.noise_dim,
           'fixed_noise': fixed_noise,
           'fixed_aux_labels_ohe': fixed_aux_labels_ohe,
           'D_loss_list': D_loss_list,
           'G_loss_list': G_loss_list,
           }
    if config.ema:
        res['netG_EMA'] = netG_EMA

    return res
