import shutil

import numpy as np
import os
import torch
from libs.utils.transform_utils import sharpen
from torchvision.transforms import transforms
from torchvision.utils import save_image

device = torch.device("cuda" if torch.cuda.is_available() else "cpu")


# truncation_trick
def biggan_submission(netG, noise_dim, model_dir, truncated=None, SHARPEN_params=None):
    im_batch_size = 50
    n_images = 10000
    output_images = model_dir + 'image_results/'

    for i_batch in range(0, n_images, im_batch_size):
        if truncated is not None:
            flag = True
            z = None
            while flag:
                z = np.random.randn(100 * im_batch_size * noise_dim)
                z = z[np.where(abs(z) < truncated)]
                if len(z) >= im_batch_size * noise_dim:
                    flag = False
            gen_z = torch.from_numpy(z[:im_batch_size * noise_dim]).view(im_batch_size, noise_dim, 1, 1)
            gen_z = gen_z.float().to(device)
        else:
            gen_z = torch.randn(im_batch_size, noise_dim, 1, 1, device=device)
        #         gen_z = gen_z / gen_z.norm(dim=1, keepdim=True)
        aux_labels = np.random.randint(0, 1, im_batch_size)
        aux_labels_ohe = np.eye(1)[aux_labels]
        aux_labels_ohe = torch.from_numpy(aux_labels_ohe[:, :, np.newaxis, np.newaxis])
        aux_labels_ohe = aux_labels_ohe.float().to(device)

        gen_images = netG(gen_z, aux_labels_ohe)
        gen_images = gen_images.to("cpu").clone().detach()  # shape=(*,3,h,w), torch.Tensor
        # denormalize
        gen_images = gen_images * 0.5 + 0.5
        for i_image in range(gen_images.size(0)):
            if SHARPEN_params:
                img = transforms.ToPILImage()(gen_images[i_image])
                img = sharpen(img, magnitude=SHARPEN_params['SHARPEN_MAGNITUDE'])
                img = transforms.ToTensor()(img)
                save_image(img, os.path.join(output_images, f'image_{i_batch + i_image:05d}.png'))
            else:
                save_image(gen_images[i_image, :, :, :],
                           os.path.join(output_images, f'image_{i_batch + i_image:05d}.png'))
    shutil.make_archive(model_dir + 'images', 'zip', output_images)
