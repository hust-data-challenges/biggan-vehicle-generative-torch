import os
import shutil
import sys

import numpy as np
import torch
import torch.backends.cudnn as cudnn
from tqdm import tqdm

from libs.datasets.dataset_factory import get_dataloader
from libs.evaluation import MiFIDEvaluator
from libs.losses.loss_factory import get_loss
from libs.models.model_factory import get_discriminator
from libs.models.model_factory import get_generator
from libs.optimizers.optimizer_factory import get_generator_optimizer, get_discriminator_optimizer
from libs.scheduler_factory import get_scheduler
from libs.transformer import get_transform
from libs.utils.common import gen_and_save, plot_losses, write_logs, make_all_dirs, make_dir
from libs.utils.logger import Logger
from libs.utils.timer import Timer

device = torch.device("cuda:0" if torch.cuda.is_available() else "cpu")


class Solver(object):
    def __init__(self, config, print_model=False):
        self.config = config
        make_all_dirs(config)
        transform1, transform2 = get_transform(config.transform, config.common)
        preprocessor = None
        data_loader = get_dataloader(config, preprocessor, transform1, transform2)
        print("======== Building Model ==========")
        vehicle_generator = get_generator(config.model, config.common)
        vehicle_discriminator = get_discriminator(config.model, config.common)

        self.use_gpu = torch.cuda.is_available()
        if self.use_gpu:
            print('Utilize GPUs for computation')
            print('Number of GPU available', torch.cuda.device_count())
            vehicle_generator = vehicle_generator.to(device)
            vehicle_discriminator = vehicle_discriminator.to(device)
            cudnn.benchmark = True
            if torch.cuda.device_count() > 1:
                vehicle_generator = torch.nn.DataParallel(vehicle_generator)
                vehicle_discriminator = torch.nn.DataParallel(vehicle_discriminator)
        vehicle_generator.apply(weights_init)
        vehicle_discriminator.apply(weights_init)

        if print_model:
            print('Generator model architectures:\n{}\n'.format(vehicle_generator))
            print("=" * 100)
            print('Generator model architectures:\n{}\n'.format(vehicle_generator))

        # train dataset
        self.train_loader = data_loader

        # model
        self.vehicle_generator = vehicle_generator
        self.vehicle_discriminator = vehicle_discriminator

        # optimizer
        self.g_optimizer = get_generator_optimizer(config.optimizer, self.vehicle_generator.parameters())
        self.d_optimizer = get_discriminator_optimizer(config.optimizer, self.vehicle_discriminator.parameters())

        # lr scheduler
        self.g_lr_scheduler = get_scheduler(config.scheduler.generator, self.g_optimizer, 0)
        self.d_lr_scheduler = get_scheduler(config.scheduler.discriminator, self.d_optimizer, 0)

        # metrics
        self.g_losser = get_loss("generator")
        self.d_losser = get_loss("discriminator")

        # set the logger
        self.g_logger = Logger(config.common.logs_dir + 'models/generation_logs')
        self.d_logger = Logger(config.common.logs_dir + 'models/discrimination_logs')
        self.fid_logger = Logger(config.common.logs_dir + 'mifids/fid_logs')
        self.distance_logger = Logger(config.common.logs_dir + 'mifids/distance_logs')
        self.mifid_logger = Logger(config.common.logs_dir + 'mifids/mifid_score_logs')
        self.output_dir = config.common.exp_dir
        self.g_checkpoint = config.common.resume_checkpoint + 'g_checkpoint.pth'
        self.d_checkpoint = config.common.resume_checkpoint + 'd_checkpoint.pth'
        self.checkpoint_prefix = config.common.checkpoint_prefix

        # set evalutor
        self.evaluator = MiFIDEvaluator(config.common.evaluation_model, config.common.train_dir)

    def save_checkpoint(self, epochs, iters=None):
        if iters:
            g_filename = self.checkpoint_prefix + '_generator_epoch_{:d}_iter_{:d}'.format(epochs, iters) + '.pth'
            d_filename = self.checkpoint_prefix + '_discrinator_epoch_{:d}_iter_{:d}'.format(epochs, iters) + '.pth'
        else:
            g_filename = self.checkpoint_prefix + '_generator_epoch_{:d}'.format(epochs) + '.pth'
            d_filename = self.checkpoint_prefix + '_discrinator_epoch_{:d}'.format(epochs) + '.pth'
        g_filename = os.path.join(self.output_dir, g_filename)
        d_filename = os.path.join(self.output_dir, d_filename)
        torch.save(self.vehicle_generator.state_dict(), g_filename)
        torch.save(self.vehicle_discriminator.state_dict(), d_filename)
        with open(os.path.join(self.output_dir, 'checkpoint_list.txt'), 'a') as f:
            f.write(f'Epoch {epochs}: {g_filename} --- {g_filename}\n')
        # self.log_writer.write('Wrote snapshot to: {:s}'.format(filename))
        print('Wrote snapshot to: {:s} and {:s}'.format(g_filename, d_filename))

    def resume_checkpoint(self, g_resume_checkpoint, d_resume_checkpoint):
        if not os.path.isfile(g_resume_checkpoint) \
                or not os.path.isfile(d_resume_checkpoint):
            print(f" => no checkpoint found at '{g_resume_checkpoint}' or '{d_resume_checkpoint}'")
            return False
        self.vehicle_generator.load_state_dict(torch.load(g_resume_checkpoint))
        self.vehicle_discriminator.load_state_dict(torch.load(d_resume_checkpoint))

    def find_previous(self):
        if not os.path.exists(os.path.join(self.output_dir, 'checkpoint_list.txt')):
            return False
        with open(os.path.join(self.output_dir, 'checkpoint_list.txt'), 'r') as f:
            lines = f.readlines()
        epochs, g_resume_checkpoints, d_resume_checkpoints = [list() for _ in range(3)]
        for line in lines:
            line_list = line.replace(':', '').replace('---', '').strip('\n').split(' ')
            epochs.append(int(line_list[1]))
            g_resume_checkpoints.append(line_list[2])
            d_resume_checkpoints.append(line_list[3])
        return epochs, g_resume_checkpoints, d_resume_checkpoints

    def weights_init(self):
        pass

    def initialize(self):
        if self.g_checkpoint and self.d_checkpoint:
            print('Loading initial model weights from {:s} and {:s}'
                  .format(self.g_checkpoint, self.d_checkpoint))
            self.resume_checkpoint(self.g_checkpoint, self.d_checkpoint)

        start_epoch = 0
        return start_epoch

    def train_model(self, n_example_to_gen, calculate_mifid, config):
        previous = self.find_previous()
        if previous:
            start_epoch = previous[0][-1]
            self.resume_checkpoint(previous[1][-1], previous[2][-1])
        else:
            start_epoch = self.initialize()

        seed = torch.randn([n_example_to_gen, config.common.noise_dim, 1, 1], device=device)
        train_config = config.train
        all_gl = np.array([])
        all_dl = np.array([])
        exp_replay = []
        for epoch in iter(range(start_epoch + 1, train_config.epochs + 1)):
            sys.stdout.write(
                '\rEpoch {epoch:d}/{max_epochs:d}:\n'.format(epoch=epoch, max_epochs=train_config.epochs))
            if 'train' in config.phase:
                self.train_epoch(epoch, train_config, self.vehicle_generator, self.vehicle_discriminator,
                                 self.g_optimizer, self.d_optimizer,
                                 self.g_losser, self.d_losser,
                                 self.g_lr_scheduler, self.d_lr_scheduler,
                                 self.train_loader, all_gl, all_dl, seed)

            if 'generate' in config.phase:
                self.gen_images()

            if epoch % calculate_mifid == 0:
                self.gen_images(self.evaluator, epoch, train_config.gen_imgs_dir)

            if epoch % train_config.decay_step == 0:
                # new_lr_d = tf.train.cosine_decay(new_lr_d, min(global_step, train_config.lr_decay_steps),
                #                                  train_config.lr_decay_steps)
                # new_lr_g = tf.train.cosine_decay(new_lr_g, min(global_step, train_config.lr_decay_steps),
                #                                  train_config.lr_decay_steps)
                # g_optimizer = tf.train.AdamOptimizer(learning_rate=new_lr_d, beta1=0.5)
                # d_optimizer = tf.train.AdamOptimizer(learning_rate=new_lr_g, beta1=0.5)
                pass
        gen_and_save(self.vehicle_generator, seed, train_config.epochs)
        self.gen_outputs(self.evaluator, train_config.epochs, train_config.gen_imgs_dir)

    def train_epoch(self, epoch, train_config,
                    generator, discriminator,
                    g_optimizer, d_optimizer,
                    g_losser, d_losser,
                    g_scheduler, d_scheduler,
                    data_loader, all_gl, all_dl, seed):

        noise = torch.randn(train_config.batch_size, train_config.noise_dim, 1, 1, device=device)

        g_losses = []
        d_losses = []

        global_step = 0
        _t = Timer()
        start = _t.toc()

        for real_images in tqdm(data_loader):
            _t.tic()
            gen_loss, disc_loss = self.train_step(noise, real_images, generator,
                                                  discriminator, g_optimizer, d_optimizer,
                                                  g_losser, d_losser)

            gen_loss.backward()
            g_optimizer.step()

            disc_loss.backward()
            d_optimizer.step()

            global_step = global_step + 1
            g_losses.append(gen_loss.item())
            d_losses.append(disc_loss.item())
            all_gl = np.append(all_gl, np.array([g_losses]))
            all_dl = np.append(all_dl, np.array([d_losses]))

        # log per epoch
        sys.stdout.write('\r')
        sys.stdout.flush()
        g_lr = g_optimizer.param_groups[0]['lr']
        d_lr = d_optimizer.param_groups[0]['lr']
        log = '\rTrain: || Total_time: {time:.3f}s || g_loss: {g_loss:.4f} ' \
              ' d_loss: {d_loss:.4f} || g_lr: {g_lr:.6f} || d_lr: {d_lr:.6f}\n'.format(
            time=_t.total_time, g_loss=sum(g_losses) / len(g_losses),
            d_loss=sum(d_losses) / len(d_losses),
            g_lr=g_lr, d_lr=d_lr)
        # self.log_writer.write(log)
        sys.stdout.write(log)
        sys.stdout.flush()

        write_logs(self.g_losser, 'generative_loss', sum(g_losses) / len(g_losses), epoch)
        write_logs(self.d_losser, 'discriminative_loss', sum(g_losses) / len(g_losses), epoch)

        if epoch % train_config.display_results:
            plot_losses(g_losses, d_losses, all_gl, all_dl, epoch)
            gen_and_save(generator, epoch, seed, rows=8, cols=8)

        # Cosine learning rate decay
        if epoch % train_config.decay_step == 0:
            g_scheduler.step()
            d_scheduler.step()

        if epoch % 100:
            torch.save(generator.state_dict(), "generator_epoch_{}.pth".format(epoch))
            torch.save(discriminator.state_dict(), "discriminator_epoch_{}.pth".format(epoch))

        print('Epoch: {} computed for {} sec'.format(epoch + 1, _t.toc() - start))
        print('Gen_loss mean: ', np.mean(g_losses), ' std: ', np.std(g_losses))
        print('Disc_loss mean: ', np.mean(g_losses), ' std: ', np.std(g_losses))

    def train_step(self, noise, real_images, generator, discriminator, g_optimizer, d_optimizer, g_losser, d_losser,
                   loss_type='gan'):
        discriminator.zero_grad()
        generator.zero_grad()

        real_images = real_images.to(device)
        fake_images = generator(noise)

        real_output = discriminator(real_images).view(-1)
        fake_output = discriminator(fake_images).view(-1)

        gen_loss = g_losser(real_output, fake_output, loss_type, apply_label_smoothing=True)
        disc_loss = d_losser(real_output, fake_output, loss_type,
                             apply_label_smoothing=True, label_noise=True)

        return gen_loss, disc_loss

    def gen_outputs(self, evaluator, epoch, gen_imgs_dir):
        make_dir(gen_imgs_dir)
        fid_value, distance, mi_fid_score = evaluator.evaluate(gen_imgs_dir)
        write_logs(self.fid_logger, fid_value, epoch)
        write_logs(self.distance_logger, distance, epoch)
        write_logs(self.mifid_logger, mi_fid_score, epoch)
        print(f'FID: {fid_value:.5f}')
        print(f'distance: {distance:.5f}')
        print(f'MiFID: {mi_fid_score:.5f}')
        shutil.rmtree(gen_imgs_dir)
        print('Removed temporary image directory.')
        print('Final epoch.')
