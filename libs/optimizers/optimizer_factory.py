import torch.optim as optim


def Adam(parameters, lr=0.001, betas=(0.9, 0.999), weight_decay=0, amsgrad=False, **_):
    if isinstance(betas, str):
        betas = eval(betas)
    # print('weight decay:', weight_decay)
    return optim.Adam(parameters, lr=lr, betas=betas, weight_decay=weight_decay, amsgrad=amsgrad)


def Sgd(parameters, lr=0.001, momentum=0.9, weight_decay=0, nesterov=True, **_):
    return optim.SGD(parameters, lr=lr, momentum=momentum, weight_decay=weight_decay, nesterov=nesterov)


def get_generator_optimizer(config, parameters):
    print(config.generator.type)
    return globals().get(config.generator.type)(parameters, config.lr_initial_g, **config.generator.params)


def get_discriminator_optimizer(config, parameters):
    return globals().get(config.discriminator.type)(parameters, config.lr_initial_d, **config.discriminator.params)
