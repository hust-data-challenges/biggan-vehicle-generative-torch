import cv2
import numpy as np
from .helper import get_tf_env

class MotorbikeClassifier():
    def __init__(self, model_path, gpu_id=-1, mem_fraction=0.2, img_size=128):
        # self.tf, self.sess, self.graph = get_tf_env(gpu_id=gpu_id, mem_fraction=mem_fraction)
        self.tf, self.config = get_tf_env(gpu_id=gpu_id, mem_fraction=mem_fraction)
        self.img_size=img_size
        self.PATH_TO_MODEL = model_path
        
        print('[Motorbike Classifer] Load Motorbike Classifer from {}'.format(self.PATH_TO_MODEL))
        self.graph = self.tf.Graph()
        self.sess = self.tf.Session(graph=self.graph, config=self.config)

        with self.graph.as_default():
            od_graph_def = self.tf.GraphDef()
            with self.tf.gfile.GFile(self.PATH_TO_MODEL, 'rb') as fid:
                serialized_graph = fid.read()
                od_graph_def.ParseFromString(serialized_graph)
                self.tf.import_graph_def(od_graph_def, name='')
            self.input_tensor = self.graph.get_tensor_by_name('input_1:0')
            self.output_tensor = self.graph.get_tensor_by_name('activation_95/Sigmoid:0')
            self.embedding_tensor = self.graph.get_tensor_by_name('global_average_pooling2d_1/Mean:0')


    def predict(self, img_expanded):    
        try:
            with self.graph.as_default():
                print(self.output_tensor)
                scores, embs = self.sess.run([self.output_tensor,  self.embedding_tensor],
                    feed_dict={self.input_tensor: img_expanded})
            return scores, embs
        except:
            import traceback
            print(traceback.print_exc())

if __name__ == '__main__':
    model = MotorbikeClassifier('./motorbike_classification_inception_net_128_v4_e36.pb')
